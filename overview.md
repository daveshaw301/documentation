A test automation and performance test framework developed by Test Design and Consultancy Services (TDCS), part of Home Office Technology

**Automation**

Current automation solution elements of the service (the “Virtuoso” framework) include:
- Automation of web browser based user interfaces (not including browser plugins such as Adobe Flash or Silverlight) using Selenium.
- Automation of web service based (SOAP and REST) interfaces.
- Definition of automated test “steps” using code (Java).
- Definition of automated test “steps” using behavior-driven development (BDD) using Cucumber.
- Flexible definition of automated test data (input and expected results), ranging from within test steps to external data sources.
- Definition of underlying test “artifacts” (such page objects, service descriptions) using code (Java).
- Integration of test automation with Continuous Integration (Jenkins).
- Seamless integration with cloud based automated compatibility testing services such as Sauce Labs or BrowserStack.
- Service simulation (stubbing) of SOAP and REST based interfaces and integration with the automation test framework to allow joined up end to end test scenario execution.

**Performance**

Current performance solution elements of the service (the “Maestro” framework) include:
- Performance testing by the simulation of load against Web UI and Web Service based interfaces at the protocol level using Apache JMeter.
- Browser level web page performance analysis from a single user perspective using WebPageTest.
- Server side (application, infrastructure, OS and database) monitoring during performance test execution.
- Integration with Continuous Integration (Jenkins) for regular low volume performance benchmarking (protocol and single user browser based).
- High volume performance testing using internal load injection infrastructure.
- High volume performance testing using external cloud based load injection infrastructure such as Amazon Web Services EC2 and Rackspace (security permitting - external services need access to the web service and test data).


## Virtuoso ##
Virtuoso is a Java library that is used to facilitate the development and maintenance of test automation suites. This is achieved in three ways:

1. Using classes to abstract common functionalities (capabilities) that are encountered in a wide range of systems under test (SUTs), e.g. UI-driven actions, database interactions, web service requests etc.
2. By using a common approach for test data input and test reporting.
3. By promoting a structured way to develop tests, which separates test logic from system-specific implementation. 

Virtuoso currently uses JUnit as its test execution engine. Test engineers that want to use Virtuoso will need to include it as a dependency to their test automation suites and extend their test classes from Virtuoso base classes (see [Requirements](requirements)). 

One of the main design principles of Virtuoso is to offer its capabilities in a transparent way. No explicit initialisation or finalization actions are required in the test classes, while a set of naming conventions are followed to automatically link input data and reports to tests. Test executions can be parameterised with a key-value fashion (see [Configuration](configuration)) to better address the needs of different automation tests. 

The following image shows the high-level architecture of the Virtuoso library. 

![Virtuoso Logical Architecture](virtuoso/images/virtuosologicalarchitecture.png)

At the top is the “Test” node, where the steps of the test script are coded and the “Test Data” that define input values and expected results for the test steps. The test steps can also be generated via Cucumber-JVM using Gherkin-syntax based feature files. Data can also be defined in feature files as “Examples”. This level is also accessible by Test Analysts, who can update test data files and feature files, as well as review the test classes, which can be designed in such a way that they hide implementation details and are appropriate to be read by non-technical resources.

The test class invokes the methods of “Target Objects” which implement the specific link to the system under test. These are developed, read, and maintained by test engineers. The interfaces to the “Target Objects” should be clear enough, so that the test classes that invoke them are easily understood.

Configuration data can be used to parameterise a test execution for various environments and contexts.

Blue-coloured nodes relate to the core framework code and need not be processed during the development of a test automation suite. They are used to offer the capabilities, test data management and reporting features of the Virtuoso framework in a transparent way.

## Maestro ##

Maestro uses the open source application Apache JMeter for the simulating of load against Web UI and Web Service based interfaces at the protocol level.

Unlike many commercial load testing tools, JMeter has very limited server side monitoring capability and therefore further tools are used to achieve this, including utilities such as sar and vmstat along with built support for the commercial AppDynamics APM tool.
 
AppDynamics is an Application Performance Management (APM) tool that allows very detailed monitoring of System under Test infrastructure, ranging from Operating System metrics to detailed Application Server and Database metrics.
 
Maestro also uses a private instance of WebPageTest for the performance testing of browser level web page performance, providing a view of performance from a single user client perspective.
 
Maestro provides “internal” (i.e. within the firewall) load injection capability, ranging from regular low volume performance smoke testing as part of continuous build and integration processes to high volume load testing from internal load injection server farms.  These load injector servers will be required to be provisioned by projects using the solution.
 
In order to provide high volume and cost effective load injection against public internet facing test environments whilst testing the full end to end infrastructure stack (and security allowing), Maestro integrates with public cloud providers such as Amazon Web Services EC2 and Rackspace, providing the ability to inject from both UK and global locations as required. 

## Vitraux ##

Vitraux is a web-server that is used to autonomously handle executions of automated (Virtuoso) or performance (Maestro) tests. It wraps existing test automation, load testing, and monitoring tools, in order to orchestrate a test execution, while gathering and reporting its results. Interfaces to different tools and technologies can be developed to extend Vitraux's capabilities and better target the specific needs of each project.
 
Two types of tests can be triggered by Vitraux, when a new execution request arrives: Automation Tests or Performance Tests.
 
**Automation:**
- Automation Tests rely on the Virtuoso library, which is a Java library that offers support for a set of commonly used test types with JUnit.
- Vitraux starts a new Virtuoso execution with the current working directory being the "test assembly directory" corresponding to the requested test name. The user account that executes the Virtuoso (JVM) process is exactly the same as the one that runs the Tomcat server and thus any required permissions to successfully run this test need to be set up correctly for that user account.
- Vitraux overrides the output directory selection of the Virtuoso config.properties file and outputs all results under a specified results directory within the "test assembly directory". At the end of the execution, this output directory is compressed into a zip file that becomes available on the Tomcat web-server under the results/ path.  
 
**Performance:**
- Performance Tests rely on the Maestro framework, which is a Java library that fully orchestrates a performance test execution by wrapping existing tools. It supports set-up and clean-up activities, monitoring set-up on the servers of the System under Test, triggering of the load test program, and gathering and presentation in tables and graphs of the performance test results. An XML file, called plan.xml, needs to be defined at the root of the registered test assembly zip file, to define all these different types of activities.
- Vitraux starts a new Maestro execution with the current working directory being the "test assembly directory" corresponding to the requested test name. The user account that executes the Maestro process is exactly the same as the one that runs the Tomcat server and thus any required permissions to successfully run this test need to be set up correctly for that user account. This user account also needs to have set-up public-key authentication towards all monitored servers, so as to automatically remote ssh and gather performance metrics without a password prompt.
- At the end of the execution, the various result files that are described by the plan.xml are compressed into a zip file that becomes available on the Tomcat web-server under the results / path.  

