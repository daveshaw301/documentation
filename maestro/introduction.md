# Introduction 

Maestro uses the open source application Apache JMeter for the simulating of load against Web UI and Web Service based interfaces at the protocol level.

Unlike many commercial load testing tools, JMeter has very limited server side monitoring capability and therefore further tools are used to achieve this, including utilities such as sar and vmstat along with built support for the commercial AppDynamics APM tool.

AppDynamics is an Application Performance Management (APM) tool that allows very detailed monitoring of System under Test infrastructure, ranging from Operating System metrics to detailed Application Server and Database metrics.

Maestro also uses a private instance of WebPageTest for the performance testing of browser level web page performance, providing a view of performance from a single user client perspective.

Maestro provides “internal” (i.e. within the firewall) load injection capability, ranging from regular low volume performance smoke testing as part of continuous build and integration processes to high volume load testing from internal load injection server farms. These load injector servers will be required to be provisioned by projects using the solution.

In order to provide high volume and cost effective load injection against public internet facing test environments whilst testing the full end to end infrastructure stack (and security allowing), Maestro integrates with public cloud providers such as Amazon Web Services EC2 and Rackspace, providing the ability to inject from both UK and global locations as required.