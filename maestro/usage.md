# Usage

The Maestro framework is a Java library that fully orchestrates a performance test execution by wrapping existing tools. It supports set-up and clean-up activities, monitoring set-up on the servers of the System under Test, triggering of the load test program, and gathering and presentation in tables and graphs of the performance test results. An XML file, called plan.xml, needs to be defined at the root of the registered test assembly zip file, to define all these different types of activities.

Maestro works with Vitraux which will start a new Maestro execution with the current working directory being the "test assembly directory" corresponding to the requested test name. The user account that executes the Maestro process is exactly the same as the one that runs the Tomcat server and thus any required permissions to successfully run this test need to be set up correctly for that user account. This user account also needs to have set-up public-key authentication towards all monitored servers, so as to automatically remote ssh and gather performance metrics without a password prompt.

At the end of the execution, the various result files that are described by the plan.xml are compressed into a zip file that becomes available on the Tomcat web-server under the results / path. If Jenkins is used for execution, then the execution results are available to view via the Maestro report which includes the graphs, tables and zip file of results.

# Maestro Test Plan (plan.xml)

The format of a performance test suite is

```
<performanceTests>
	<performanceTest>
		<setup>
			<script>...</script>
		</setup>
		<cleanup>
			<script>...</script>
		</cleanup>
		<test>
			<jmeterTest ... />
			<webpageTest ... />
			<ScriptedTest ... />
		</test>
		<monitoredHost>
			<topmonitor output="..."/>
			<vmstatmonitor output="..."/>
			<sarmonitor output="..."/>
		</monitoredHost>
		<requirements>
			<requirement type="..."/>
		</requirements>
		<graphs>
			<graph type="..." ... />
		</graphs>
		<tables>
			<table type="..." ... />
		</tables>
	</performanceTest>
	<performanceTest>
	...
	...
	</performanceTest>
</performanceTests>
```

## performanceTest
Defines the performance test, made up of the components below. There can be multiple performance tests within the suite

## setup
Defines any setup scripts to run before the test execution

### Examples
Output a message to the Jenkins console
```xml
<script>echo A new JMeter test is starting</script>
```

Run a script from the /scripts folder
```xml
<script>./scripts/load-test-data.sh</script>
```

## cleanup
Defines any cleanup scripts to run after the test execution

### Examples
Same format as the setup scripts

## JMeter
Defines a JMeter performance test

### Parameters

|Attribute|Description|Required|
|---------|-----------|--------|
|title|The title of the performance test, e.g. "Peak Load Test"|**Yes**|
|plan|The JMeter Test plan (jmx file)|**Yes**|
|output|The directory to output temporary test results|**Yes**|
|remoteHosts|A comma separated list of hosts to connect to for distributed load testing|**No**|

### Examples
Run a JMeter test described by by file plan.jmx
```xml
<jmeterTest title="Stress Test" plan="plan.jmx" output="results/"/>
```

Run a JMeter test on 3 slave hosts. Note that the slave hosts must have a jmeter server instance started and listening on the specified ports for the master host to be able to connect successfully.
```xml
<jmeterTest title="Stress Test" plan="plan.jmx" output="results/" remoteHosts="10.0.2.15,10.0.2.16:12006,10.0.2.17:1099"/>
```

## WebPageTest
Definds a WebPageTest performance test

### Parameters

|Attribute|Description|Required|
|---------|-----------|--------|
|title|The title of the performanace test, e.g. "Peak Load Test"|**Yes**|
|url|The URL to run the WebPageTest against|**Yes**|
|server|The WebPageTest server|**Yes**|
|location|The location ot run the test from. The location should be a valid in the selected WebPageTestServer|**Yes**
|output|The file to be used for temporary test result storage|**Yes**|

### Examples
Run a WebPageTest from location EU-WEST that is defined at the local server
```xml
<webpagetest title="Launch Page Test" server="localhost" location="EU-WEST" output="results/wptOutput"/>
```

## ScriptedTest
Definds a requirement for the results of a performance test based on the results output on a file.

### Parameters

|Attribute|Description|Required|
|---------|-----------|--------|
|type|The type of requirement linking to the tool that created the file|**Yes**<ul><li>Values accepted are `vmstat`, `top`, `sar`, `jmeter`, `webpagetest`</ul>|
|category|The specific category of the requirement for the selected `type`|**Yes**<ul><li>For type `vmstat` the accepted values are `averageCpuUtilization`, `minimumFreeMemory` and `totalMemoryConsumption`<li>For type `top` the accepted values are `averageProcessCpuUtilization`</ul>|
|source|The (relative path) name of the file with the results that are to be used to check the requirement|**Yes**|
|label|A label that relates to the requirement being checked|**Yes**|
|value|The numeric value to be used as a threshold to decide whether the requirement is satisfied|**Yes**|

### Examples

Copy a single file
```xml
<copy file="myfile.txt" tofile="mycopy.txt"/>
```

Copy a single file to a directory
```xml
<copy file="myfile.txt" todir"../some/other/dir"/>
```

Copy a directory to another directory
```xml
<copy todir="../new/dir">
	<fileset dir="src_dir"/>
</copy>
```

Copy a set of files to a directory
```xml
<copy todir="../dest/dir">
	<fileset dir="src_dir">
  		<exclude name="**/*.java"/>
	</fileset>
</copy>

<copy todir="../dest/dir">
	<fileset dir="src_dir" excludes="**/*.java"/>
</copy>
```

Copy a set of files to a directory, appending .bak to the file name on the fly
```xml
<copy todir="../backup/dir">
	<fileset dir="src_dir"/>
	<globmapper from="*" to="*.bak"/>
</copy>
```

Copy a set of files to a directory, replacing @TITLE@ with Foo Bar in all files.
```xml
<copy todir="../backup/dir">
	<fileset dir="src_dir"/>
	<filterset>
		<filter token="TITLE" value="Foo Bar"/>
	</filterset>
</copy>
```

Collect all items from the current CLASSPATH setting into a destination directory, flattening the directory structure.
```xml
<copy todir="dest" flatten="true">
	<path>
		<pathelement path="${java.class.path}"/>
	</path>
</copy>
```

Copies some resources to a given directory.
```xml
<copy todir="dest" flatten="true">
	<resources>
		<file file="src_dir/file1.txt"/>
		<url url="http://ant.apache.org/index.html"/>
	</resources>
</copy>
```

If the example above didn't use the flatten attribute, the <file> resource would have returned its full path as source and target name and would not have been copied at all. In general it is a good practice to use an explicit mapper together with resources that use an relative path as their names.

Copies the two newest resources into a destination directory.
```xml
<copy todir="dest" flatten="true">
	<first count="2">
	<sort>
		<date xmlns="antlib:org.apache.tools.ant.types.resources.comparators"/>
		<resources>
			<file file="src_dir/file1.txt"/>
			<file file="src_dir/file2.txt"/>
			<file file="src_dir/file3.txt"/>
			<url url="http://ant.apache.org/index.html"/>
		</resources>
	</sort>
	</first>
</copy>
```

## MonitoredHost

Defines a host to be monitored providing as attributes the connection information. The element inside a monitored host correspond to the monitors that will be used on the host, after connecting via ssh with the provided connection information.

### Parameters

|Attribute|Description|Required|
|---------|-----------|--------|
|host|The host to connect|**Yes**|
|username|The username to connect to the host|No<ul><li>A default `~/.ssh_config` file could have the connection info to the host</ul>|
|certificate|The identify file to use to connect to the host|No<ul><li>A default `~/.ssh_config` file could have the connection info to the host</ul>|
|port|The port to use to connect via ssh<br>(if not the default 22)|No|
|config|the shh configuration file to use<br>(if not the default `~/.ssh_config`|No|

### Examples

Monitor the localhost
```
<monitoredHost host="localhost" >
	<sarmonitor ... />
	<vmstatmonitor ... />
</monitoredHost>
```

Monitor host `application01.mydomain.com` using the identity file `cert.pem`
```
<monitoredHost host="application01.mydomain.com" certificate="cert.pem" >
	<sarmonitor ... />
	<vmstatmonitor ... />
</monitoredHost>
```

Monitor host `172.21.23.46` using the non-default configuration file `conf/my_ssh_config`
```
<monitoredHost host="172.21.23.46" config="conf/my_ssh_config" >
	<sarmonitor ... />
	<vmstatmonitor ... />
</monitoredHost>
```

## Requirement
Defines a requirement for the results of a perfomance test based on the results output on a file.

### Parameters

|Attribute|Description|Required|
|---------|-----------|--------|
|type|The type of requirement linking to the tool that created the file|**Yes**<ul><li>Values accepted are `vmstat`, `top`, `jmeter` and `webpagetest`</ul>|
|category|The specific category of the requirement for the selected `type`|**Yes**<ul><li>For type  `vmstat` the accepted values are `averageCpuUtilization`, `minimumFreeMemory`, and `totalMemoryConsumption`<li>For type `top` the accepted values are `averageProcessCpuUtilization`<li>For type `jmeter` the accepted values are `averageTransactionTime`, `averageResponseTime`, `globalErrorRate` and `transactionErrorRate`<li>For type `webpagetest` the accepted values are `firstPageLoadTime` and `repeatViewPageLoadTime`</ul>|
|source|The (relative path) name of the file with the results that are to be used to check the requirement|**Yes**|
|label|A label that relates to the requirement being checked|**Yes**|
|value|The numeric value to be used as a threshold to decide whether the requirement is satisfied|**Yes**|

### Categories
|Type|Category|Description|Attributes|
|----|--------|-----------|----------|
|vmstat|averageCpuUtilization|Average CPU utilisation percentage should be less than value|value from `0.0` to `100.0`|
|vmstat|minimumFreeMemory|Minimum free memory in MB should be more than value|value in `MB`|
|vmstat|totalMemoryConsumption|Maximum memory consumed during the execution of the performance test(maximum - minimum) should be less than value|value in `MB`|
|top|averageProcessCpuUtilization|Average CPU utilisation percentage of a specific process should be less than value|`process` string to be matched against the system running processes, value from `0.0` to `100.00`|
|jmeter|averageTransactionTime|Average time amongst all transactions (organised as transaction controller in the JMeter script) that are stored in the defined output file should be less than value|value in `milliseconds`|
|jmeter|averageResponseTime|Average time amongst all requests (organised as samplers in the JMeter script) that are stored in the defined output file should be less than value|value in `milliseconds`|
|jmeter|globalErrorRate|The global error rate amongst all different transactions should be less than value|value in percentage from `0.0` to `1.0`|
|webpagetest|firstPageLoadTime|The time of a page without any caching should be less than value|value in `seconds`|
|webpagetest|repeatViewPageLoadTime|The time of a page with caching should be less than value|value in `seconds`|

### Examples

The total memory consumption as reported by vmstat in the file vmstatMongodb02 should be less than 256MB

```xml
<requirement type="vmstat" category="totalMemoryConsumption" source="results/vmstatMongodb02" label=mongodb02" value="256.0" />
```

## Graph
Defines a graph to be generated from the results of a performance test. A source file attribute defines the files from where to read the raw data and the type and category tuple that determines how these data are going to be processed. Source files can either be ones generated during the execution of the performance test, or previously existing files (e.g. baseline file results) and one might want to present together with new results.

Graphs can be combined into a composite graph, using a common title, and different 'line' elements for each axis 'y1' and 'y2'. The 'line' elements have exactly the same attributes as the typical 'graph' ones.


Monitor the localhost

### Parameters

|Attribute|Description|Required|
|---------|-----------|--------|
|type|The type of requirement linking to the tool that created the file|**Yes**<ul><li>Value accepted are `vmstat`, `top`, `sar` and `jmeter`</ul>|
|category|The specific category of requirement for the selected `type`|**Yes**<br><ul><li>For type `vmstat` the accepted values are `cpuUtilisation`, `memUsage`, `diskBlocksRead`, and `diskBlocksWritten`<li>For type `top` the accepted values are `cpuUtilisation`<li>For type `sar` the accepted values are `networkOutboundBandwidth` and `networkInboundBandwidth`<li>For type `jmeter` the accepted values are `load`, `errors`, `averageTransactionTimes` and `averageRequestTimes`</ul>|
|source|The (relative path) name of the file with the results that are to be used to check the requirement|**Yes**|
|label|A label that relates to the graph. It will be used as the graph title.|**Yes**|

### Categories

|Type|Category|Description|Attributes|
|----|--------|-----------|----------|
|vmstat|cpuUtilisation|CPU utilisation of the server| |
|vmstat|memUsage|Available memory on the server| |
|vmstat|diskBlocksRead|Disk blocks read/sec on the server| |
|vmstat|diskBlocksWritten|Disk blocks written/sec on the server| |
|top|cpuUtilisation|CPU utilisation of a specific process|"process" string to be matched against the system running processes|
|sar|networkOutboundBandwidth|Outbound traffic/sec on the server from a specific interface|(Optional) "interface": specify the name of the network interface to monitor|
|sar|networkInboundBandwidth|Disk blocks written/sec on the server|(Optional) "interface": specify the name of the network interface to monitor|
|jmeter|load|The total concurrent virtual users (JMeter threads)|(Optional) "hostCount": Specify an integer value the amount of load injectors used. Defaults to 1.|
|jmeter|errors|The erroneous responses against time| |
|jmeter|averageTransactionTimes|The average times of the transactions as defined by the transaction controllers in the JMeter script that generated output| |
|jmeter|averageRequestTimes|The average times of the requests as defined by all samplers in the JMeter script that generated output| |


### Examples
Graph the memory usage of AppServer02, using vmstat measurements found in results/vmstatAppServer02

```xml
<graph type="vmstat" category="memUsage" source="results/vmstatAppServer02" label="AppServer02"/>
```

Graph the average JMeter transaction times, using JMeter exported results found in results/jmeterRes.csv
```xml
<graph type="jmeter" category="averageTransactionTimes" source="results/jmeterRes.csv" label="Average Transaction Times"/>
```

Composite graph showing the vmstat acquired CPU usage on y1 against JMeter concurrent virtual users on y2
```xml
<graph label="mongodb03 vs concurrent virtual users">
	<y1 type="vmstat" category="cpuUtilisation" source="results/vmstatMongodb03" label="MongoDB03"/>
	<y2 type="jmeter" category="load" 	  source="scripts/results/table.jmx" "Concurrent Virtual Users"/>
</graph>
```

Composite graph showing multiple vmstat acquired CPU usages (baseline and current) from a server on y1 against JMeter concurrent virtual users on y2
```xml
<graph label="MongoDB01 (baseline and current) vs concurrent virtual users">
	<y1>
		<line type="vmstat" category="cpuUtilisation" source="results/vmstatMongodb01Baseline" label="Baseline"/>
		<line type="vmstat" category="cpuUtilisation" source="results/vmstatMongodb01" label="Current"/>
	</y1>
	<y2 type="jmeter" category="load" 	  source="scripts/results/table.jmx" "Concurrent Virtual Users"/>
</graph>
```

## Table
Defines a table in Comma Separated Value (CSV) for the results of a performance test based on the results in a file.

### Parameters
|Attribute|Description|Required|
|---------|-----------|--------|
|type|The type of requirement linking to the tool that created the file.|**Yes**<ul><li>Values accepted are `top`, `jmeter`, and `webpagetest`|
|category|The specific category of requirement for the selected type.</ul>|**Yes**<ul><li>For type `top` the accepted values are `topCpuUtilisationProcesses`<li>For type `jmeter` the accepted values are `transactionStats` and `errorTypeStats`<li>For type `webpagetest` the accepted values are `resultsLink`</ul>|
|source|The (relative path) name of the file with the results that are to be used to generate the table.|**Yes**|
|label|A label that relates to the table to be generated.|**Yes**|

### Categories
|Type|Category|Description|Attributes|
|----|--------|-----------|----------|
|top|topCpuUtilisationProcesses|Table with the five processes with the highest average CPU utilisation on the server as reported by top||
|jmeter|transactionStats|Table with the count, the average, the 95th and 99th percentiles of all transactions (groups of requests as determined by JMeter Transaction Controllers) as reported by the "Results Table" listener of JMeter||
|jmeter|errorTypeStats|Table with all errors (non-200 responses) as reported by JMeter "Results Table" listener||
|webpagetest|resultsLink|Table with a link to the results of a WebPageTest||

### Examples
Generate the transactions table for JMeter

```xml
<table type="jmeter" category="transactionStats" source="results/table.jmx" label="Peak Load Test Transactions"/>
```

# Execution
Maestro tests are run from [Jenkins](/jenkins-plugin/configuration.md#vitraux-maestro-performance) which will execute the performance test on the [Vitraux Maestro Server](/vitraux/introduction.md#performance-vitraux-maestro)

