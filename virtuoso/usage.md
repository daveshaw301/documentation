# Test Data
## Loading Test Data
Test data is loaded automatically by the framework based on a combination of naming conventions and configuration parameters.

Test data is defined in Excel spreadsheets with two worksheets: one for input values and one for expected results. The separation is a logical one, between values that are used as inputs to proceed to the next steps of the test and values that are checked as part of the test. The names of the worksheets are configurable. Both worksheets need to be non-empty if a spreadsheet is supplied for a test.

Each worksheet is structured as follows:
- The first row has the data keys/headers
- The second and optionally any other rows have sets of values for that key/header (see Usage – Examples – Tutorial). Each test execution uses the key row and one of the values rows. The selection of the row is explained in the Repeating the same test over multiple data section.

Data can also be defined in a column-wise approach, where the first column has the keys and the subsequent ones the values. This option is set via the “Data.Orientation” configuration parameter (see Configuration).
Multiple spreadsheets with data can be loaded for each test. For example, for a method TestMethod that belongs to a class TestClass and with configuration properties:

```
Data.Dir=test-data
Data.Format=xlsx
Data.Common=ApplicationGeneric,Users
Data.TestClass.Specific=ClassSpecific
Data.TestMethod.Specific=SomeOtherTestMethodSpecific
```

data will be loaded from the following files (in order):

1. test-data/ApplicationGeneric.xlsx
2. test-data/Users.xlsx
3. test-data/ClassSpecific.xlsx
4. test-data/SomeOtherTestMethodSpecific.xlsx
5. test-data/TestMethod.xlsx

Key-values that exist in more than one file will be overwritten in the order that these files are parsed.

In theory, all data could be defined in the test-data/TestMethod.xlsx file which is loaded by default. However, the supported parameters allow for flexibility and data reuse possibilities, where common data are factored out and reused:

- along the application (Data.Common),
- along the test class (class-specific), or
- borrowed from another test method (test method-specific).

Test data can be accessed via the “testData” object that is inherited by the Virtuoso base test classes (see Requirements). Test data can be parsed as follows:

- testData.getInputValue(“key”) for values that are defined in the input values worksheet.
- testData.getExpectedResults(“key”) for values that are defined in the expected results worksheet.

## Repeating the same test over multiple test data
The org.junit.runners.Parameterized.Parameters annotation is used to select a specific set of test data values from the multiple sets of values for each key, i.e. multiple rows (or columns, if Data.Orientation=Column, henceforth only rows will be considered for brevity) for Excel spreadsheets.

A static collection of key-value pairs can be set in the test class code to set the multiple possible executions of each test method. Each pair keyN-valueN means that for the N-th repetition of each test method, the row that will be used will be the one that has the value of keyN being equal to valueN. If valueN is not unique per column, i.e. multiple rows under the keyN column have the same value, then it is advised that a new unique identifier is added to the test data to properly make the data selection.

An additional constructor that takes two strings (key and value) as arguments and that invokes the equivalent parent class constructor also needs to be defined.

An example is as follows:
```java
@RunWith(Parameterized.class)
public class StringComparisonTest extends BaseTest {

  @Parameters(name = "{index}: {0}={1}")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {
        {"Course.Id", "108"},
        {"Course.Id", "104"},
        {"Student", "admin"}});
  }

  public StringComparisonTest(String key, String value) {
    super(key, value);
  }

  @Test
  public void courseTitleTest() {
    assertor.assertEquals("Course title is not the expected one", testData.getInputValue("Title"), testData.getExpectedResult("Title"));
  }
}
```

Assuming that the “Input Values” worksheet of the test data file <test-data directory>/courseTitleTest.xlsx is as follows:

| Student | Course.Id | Title |
| ------- | --------- | ----- |
|admin|101|Databases|
|arebon|104|Design Patterns|
|arebon|105|Compilers|
|nbaltas|108|Programming|
|nbaltas|109|Algorithms|

then the courseTitleTest will be repeated three times:
- the first iteration of the test will select the row where the Course.Id value equals to 108: {nbaltas, 108, Programming}
- the second iteration of the test will  select the row where the Course.Id value equals to 104: {arebon, 104, Design Patters}
- the third iteration of the test will select the row where the Student value equals to “admin”: {admin, 101, Databases}
If multiple test-data files are loaded (see Test Data – Loading Test Data), then the key-value selection will happen for each file separately.

## Pre-Determined Test Data Keys (SauceLabs & SeleniumGrid Configuration)
Pre-Determined test-data keys can be used to configure the execution of Virtuoso test suites from cloud-based services & private instances (e.g. Saucelabs, SeleniumGrid private instance) and support compatibility testing. Specifically:

| Key | Description | Acceptable values | Required | Default |
| --- | ----------- | ----------------- | -------- | ------- |
|Configuration.Browser.Type|The browser to use.|Chrome, IE, Internet Explorer, Safari, Android, iPhone, iPad.|Optional|Firefox|
|Configuration.OS|The OS to open (SauceLabs VM, SeleniumGrid Node). 'ANY' is acceptable.|OS name. See the capabilities appearing for the “platform” value in the SauceLabs configurator: https://docs.saucelabs.com/reference/platforms-configurator/.|Mandatory||
|Configuration.Browser.Version|The version of the browser to open (SauceLabs VM, SeleniumGrid Node). 'ANY' is acceptable.|Version string. See the capabilities appearing for the “version” value in the SauceLabs configurator:https://docs.saucelabs.com/reference/platforms-configurator/.|Mandatory||
|Configuration.Browser.Width|The width of the browser window to open.|Integer (pixels)|Optional||
|Configuration.Browser.Height|The height of the browser window to open.|Integer (pixels)|Optional||


# Test Reports
## Generating Test Reports

Test reports are generated automatically by the framework based on a combination of configuration parameters, pre-determined action descriptions, and user-provided information.

Every assertion or Selenium driver action have predetermined strings assigned to them that are added to the report. Whenever they are invoked, Virtuoso will keep a log of the actions that took place to add them to the report, including user-defined strings that better describe the specific actions. This can happen in two ways:

- Assertor messages: Assertions should be done via the “assertor” object, which is inherited by the Virtuoso base test classes. Consistent to the interface of static JUnit assertion methods, the assertor takes as a first argument a string that determines what is being checked in that assertion, or specifically, the message that should appear if the assertion fails. For example:

```java
assertor.assertTrue("Expected sum is wrong", testData.getIntExpectedResult("Sum") == result);
```
- Page element objects: The last argument of page element objects is a text description of the element. Using this description, Virtuoso can generate a clear report message when a Selenium action is triggered on that element. For example, assuming the element:

```java
private static final PageElement
   SEARCH_INPUT_TEXT = new PageElement(
      PageElement.Type.ID, "searchInput", "Wikipedia Home Page: Search Input Text"
   );
```

and the action:

```java
public void setSearchInputText(String value) {
   pilot.sendKeys(SEARCH_INPUT_TEXT, value);
}
```
then Virtuoso can generate a log “ui action: set text (Wikipedia Home Page: Search Input Text, <value used in the setSearchInputText method invocation>)”. As the page elements need only be defined once, this approach can create clear and concise step-by-step descriptions of tests, which can significantly facilitate the investigation of failing tests.
A sample test report (in text format) generating in this approach is as follows:

```
Test:     wikipediaPassTest
Status:   PASS
Started:  2015.05.17 17:29:21
Ended:    2015.05.17 17:29:29
Trace:    12 step(s)
   1) [2015.05.17 17:29:24]: ui action: navigate to(http://www.wikipedia.org/)
   2) [2015.05.17 17:29:24]: ui action: get current url()
   3) [2015.05.17 17:29:24]: assert: Wrong home page URL(pass)
   4) [2015.05.17 17:29:24]: ui action: get title()
   5) [2015.05.17 17:29:24]: assert: Wrong home page title(pass)
   6) [2015.05.17 17:29:24]: ui action: set text(Wikipedia Home Page: Search Input Text,computing)
   7) [2015.05.17 17:29:24]: ui action: click(Wikipedia Home Page: Go Button)
   8) [2015.05.17 17:29:28]: ui action: get current url()
   9) [2015.05.17 17:29:28]: assert: Wrong search result page URL(pass)
  10) [2015.05.17 17:29:28]: ui action: get title()
  11) [2015.05.17 17:29:28]: ui action: get text(Wikipedia Computing Page: First Heading)
  12) [2015.05.17 17:29:28]: assert: Wrong search result page heading(pass)
```

The specific types of test reports that will be generated, i.e. txt, xls, or XML, can be configured in the parameters of the test, as explained in Configuration.

# Guidelines for writing tests in Virtuoso
Tests are developed by test engineers in Java. Although the developers are free to use the capabilities of Virtuoso in any way that better fits their automation needs, there are a few guidelines that support maintenance and readability of Virtuoso tests. These are listed here.

## Separation of test logic and implementation
One of the key principles is that the test logic, including steps, actions and test-data usage take place at the test class level or in methods shared by test classes. These inherit from the Virtuoso base test classes.

The specific implementation of the actions that interact with the framework's capabilities and in particular with page objects are recommended to be defined in separate classes that inherit from the BasePage Virtuoso class. Methods should be properly named in English, so that their invocations from the test classes are sufficient to describe the steps of the tests.

## Web site page object architecture
- Name the class in such a way as to enable discovery and reuse across the test engineering team. Linking it to page titles or activities might be recommended.
- Define the page elements as static PageElement objects. In some cases it is useful to statically define strings of xpath templates and then dynamically define the PageElement object, based on an input value from the test class.
- Use setter and getter prefixes to denote text input/output actions on the page.
- Use “click” or “select” prefixes to denote clicking or drop-down selection actions.
- Avoid defining behaviour within the page objects. Keep the abstraction clean, by only defining ways to interact with the page in units, rather than in long series of actions.
- Actions that would load a different page should return a new object of the page that will be loaded. This facilitates the understanding of page transitions as a result of the test script steps.

## Grouping of tests
Each test class can contain multiple tests (methods annotated with @org.junit.Test). For larger automation suites, it is recommended that they all follow some method naming conventions, that would also bring the corresponding test data files close to each other (in lexical sorting).

Test classes can be grouped in test suites. Virtuoso reports the results of all tests from all classes within the test suite in the same report file.

## Tips
- Use System.out.println(testData) to confirm the data that have been loaded from a test script.
- Group common actions in an easily re-usable format across test classes.
- Consider a hierarchy of page objects to re-use common elements across web pages, like navigation menus, search fields etc.

## BDD
Version 1.4 of Virtuoso supports Behaviour-Driven Development via integration with Cucumber. Test scenarios defined in feature files using the Gherkin syntax can take advantage of the Virtuoso capabilities while being executed by Cucumber.

The base runner class of the Cucumber framework is extended to link each test to be executed with the Virtuoso library. This extension takes place within the Virtuoso library, thus leaving the default execution engine of Cucumber unmodified to parse feature files and generate test cases. No changes are thus required to the Cucumber-related dependencies of a project or to its feature files in order for it to use Virtuoso.

The following actions are required to use BDD with Virtuoso:

- The default Cucumber test runner class needs to be replaced with the VirtuosoCucumber class in the Virtuoso library.
- Similarly to the typical use of the Virtuoso library, any class-defining test steps (which in the context of Cucumber comprises defining methods with @cucumber.api.java.en.Given, @cucumber.api.java.en.When, @cucumber.api.java.en.Then etc. annotations) should be a subclass of a Virtuoso base test class (see [Requirements](https://gitlab.com/tdcs_framework/virtuoso/wikis/requirements)). Any annotation options for the test runner can remain the same.

Data that are set as "Examples" in the feature files trigger multiple test executions with each being reported as a separate test case in the Virtuoso logs. Test data, loaded as described in [loading test data](#loading-test-data), can still be used and accessed via the “testData” object inherited by the Virtuoso base classes. This could relate to common Application-wide test data that do not relate to specific feature files. The separation between Virtuoso loaded test data (from spreadsheets) and Cucumber loaded test data (from feature files) is left to the discretion of the test engineer.
