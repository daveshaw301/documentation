This document will walk-through the steps needed to start using the Framework with an existing project. Further details of Virtuoso can be found at [Virtuoso Wiki](/README.md)

## Installation
Make sure you have followed the [installation](/virtuoso/installation.md) steps so that you have a copy of the virtuoso.jar in your local maven repository

## Maven
Add the virtuoso dependency to the `pom.xml`

```
<dependency>
  <groupId>uk.gov.homeoffice</groupId>
  <artifactId>virtuoso</artifactId>
  <version>1.4</version>
</dependency>
```

## Configuration
Add a configuration file to the project as `src/test/resources/config.properties`, see [Configuration](virtuoso/configuration.md) for further properties within the configuration

```
################################################################################
## Test Data                                                                  ##
################################################################################
Data.HierarchySeparator=.
Data.Format=xlsx
Data.Dir=src/test/resources/test-data
Data.InputWorksheet=Input Values
Data.ExpectedWorksheet=Expected Results

################################################################################
## Report Generation                                                          ##
################################################################################
Results.Dir=test-results
Results.Format=txt

################################################################################
## Web Driver                                                                 ##
################################################################################

WebBrowser=Firefox
Screenshots=true
Screenshots.Dir=test-results/screenshots
```



## Virtuoso Base Test Class
The test classes should subclass one of the Virtuoso base test classes offered by the framework. These are:

| Base class | Type of test |
| :--------- | :----------- |
|BaseTest|Any type of test that does not fall under any of the more specific categories below.|
|SeleniumTest|Tests that use the Selenium Web Driver to perform UI automated testing. The Selenium Web Driver is spawned before the execution of each of test method of a class that extends the SeleniumTest base class.|

- The page objects used in Selenium tests should extend the BasePage class of Virtuoso and define PageElement objects. This allows for the automated tracing and reporting at the back of the page actions.
- The host objects for Connectivity tests should extend the Host class of Virtuoso.

## JUnit Assert
The default assertions from JUnit `Assert.` should be replaced with the `assertor.` object which is inherited by the Virtuoso base test class.

Consistent to the interface of static JUnit assertion methods, the assertor takes as a first argument a string that determines what is being checke din that assertion, or specifically, the message that should appear if the assertion fails. For example

```java
assertor.assertTrue("Expected sum is wrong", testData.getIntExpectedResults("Sum")==result);
```

Find & replace can be used within the IDE to update the assertions.

## Selenium

### Pilot

Instead of calling Selenium commands directly, the call should be facilitated through Virtuoso using `pilot.` which uses

For example

```java
// Enter the query string "Cheese"
WebElement query = driver.findElement(By.name("q"));
query.sendKeys("Cheese");
```

Could become
```java
private static final PageElement SEARCH_BAR = PageElement.byName("q", "Home Page search bar");
pilot.sendKeys(SEARCH_BAR, "Cheese")
```

The following commands are taken from the Virtuoso 1.4 documentation

|**Method**|**Description**|
|:-------- |:------------- |
|`acceptAlert()`|Accept browser alert.|
|`clear(PageElement element)`|Clear a specified page's element.|
|`clearAndSendKeys(PageElement element,`<BR>`java.lang.String value)`|Clears the contens of a field before sending the specified keys.|
|`click(PageElement element)`|Click on a specified page's element.|
|`clickAndWait(PageElement element)`|Click on a specified page's element and wait for the document.readystate to be complete|
|`close()`|Close the current browser page.|
|`deleteAllCookies()`|Delete all cookies held on the browser.|
|`dismissAlert()`|Dismiss browser alert.|
|`doubleClick(PageElement element)`|Double click on a specified page's element.|
|`elementExists(PageElement element)`|Checks if the given element exists anywhere on the page.|
|`executeJavaScript(java.lang.String js,`<BR>`java.lang.Object... args1)`|Execute a fragment of JavaScript on the browser.|
|`getAllWindowHandles()`|Return all names of Windows Handles|
|`getAttribute(PageElement element,`<BR>` java.lang.String attribute)`|Obtain the textual value of the specified attribute of an element.|
|`getCookie(java.lang.String name)`|Obtain a cookie that is set in the current driver|
|`getCurrentUrl()`|Obtain the URL of the current browser page.|
|`getCurrentWindowHandle()`|Get the Current Window Handle, this allows for switching windows|
|`getMatches(PageElement element)`|Obtain a list of WebElements matching specified page's element.|
|`getPageSource()`|Obtain the source (HTML) code of the current browser page.|
|`getScreenshot()`|Capture a screenshot of the current browser page.|
|`getText(PageElement element)`|Obtain the text associated to a specified page's element.|
|`getTitle()`|Obtain the (HTML) title of the current browser page.|
|`getValue(PageElement element)`|Obtain 'value' attribute text associated to a specified page's element.|
|`getWebDriver()`|Get the underlying WebDriver instance.|
|`getWebDriverWait()`|Get the underlying WebDriverWait instance, for explicit waits.|
|`isDisplayed(PageElement element)`|Verify whether a specified page's element is displayed on the current page or not.|
|`isEnabled(PageElement element)`|Verify whether a specified page's element is enabled on the current page or not.|
|`isSelected(PageElement element)`|Verify whether a specified page's element is selected on the current page or not.|
|`navigateBack()`|Navigate back to the immediately preceding page in browsing history, if any.|
|`navigateForward()`|Navigate forward to the immediately following page in browsing history, if any.|
|`navigateRefresh()`|Refresh the page currently displayed in the browser.|
|`navigateTo(java.lang.String url)`|Navigate to a specified URL.|
|`navigateTo(java.net.URL url)`|Navigate to a specified URL.|
|`quit()`|Quit the underlying Selenium driver.|
|`saveScreenshot(java.lang.String`<BR>`screenshotFileName)`|Capture a screenshot of the current browser page.|
|`selectValue(PageElement element,`<BR>`java.lang.String value)`|Select value from dropdown.|
|`selectValueFromField(PageElement element,`<BR>`java.lang.String value)`|Selects a specified option element from within a select element|
|`sendKeys(java.lang.CharSequence...`<BR>`keysToSend)`|Send a sequence of characters to the browser.|
|`sendKeys(PageElement element,`<BR>`java.lang.String value)`|Send a sequence of characters to a specified page's element.|
|`setImplicitWait(int waitInSeconds)`|An implicit wait is to tell WebDriver to poll the DOM for a certain amount of time when trying to find an element or elements if they are not immediately available.|
|`switchToActiveElement()`|Switch focus to the active element on the browser.|
|`switchToAlert()`|Switch focus to the active alert on the browser.|
|`switchToDefaultContent()`|Switch focus to the default content on the browser.|
|`switchToFrame(int id)`|Switch focus to a specified frame.|
|`switchToFrame(PageElement element)`|Switch focus to a specified frame.|
|`switchToFrame(java.lang.String id)`|Switch focus to a specified frame.|
|`switchToWindowHandle(java.lang.String`<BR>`windowHandleName)`|Switch to Window with the supplied handle name|
|`waitForElementNotPresent(`<BR>`PageElement element)`|Wait for given element to not be visible.|
|`waitForElementPresent(`<BR>`PageElement element)`|wait for element to be displayed.|
|`waitForPageToLoad()`|Wait for page DOM to be ready.|
|`waitUntil(org.openqa.selenium.support.ui.`<BR>`ExpectedCondition<?> condition)`|Explicit wait until a condition is met.|

### Web Driver

Instead of creating an instance of webdriver directly such as

```java
public class Example  {
    public static void main(String[] args) {
        // Create a new instance of the html unit driver
        // Notice that the remainder of the code relies on the interface,
        // not the implementation.
        WebDriver driver = new FirefoxDriver();

        // And now use this to visit Google
        driver.get("http://www.google.com");

        driver.quit();
    }
}
```

You would replace with the follow two classes

```java
public class GoogleTest extends SeleniumTest {

  @Test
  public void GoogleTest() {
    GoogleHomePage googleHomePage = GoogleHomePage.launch();
  }
}
```

```java
public class GoogleHomePage extends BasePage {

  private static final String URL = "http://www.google.com";

  public static GoogleHomePage launch() {
    ExecutionContext.getInstance().getPilot().navigateTo(URL);
    return new GoogleHomePage();
  }
}
```

The browser is configured by the config.properties and when the test finishes, the session will automatically be closed.

### Page Elements
The last argument of page element objects is a text description of the element. Using this description, Virtuoso can generate a clear report message when a Selenium action is triggered on that element. For example, assuming the element

```java
private static final PageElement SEARCH_INPUT_TEXT =
new PageElement(PageElement.Type.ID, "searchInput", "Wikipedia Home Page: Search Input Text");
```

and the action

```java
public void setSearchInputText(String value) {
  pilot.sendKeys(SEARCH_INPUT_TEXT, value);
}
```

Virtuoso can generate a log

```java
ui action: set text (Wikipeida Home Page: Search Input Text,
<Value used in the setSearchInputText method invocation>)
```
As the page elements need only be defined once, this approach can create clear and concise step-by-step descriptions of tests, which can significantly facilitate the investigation of failing tests.

## Cucumber
Test scenarios defined in feature files using the Gherkin syntax can take advantage of the Virtuoso capabilities while being executed by Cucumber. The test runner class of the Cucumber framework needs to be extended to link each test to be executed with the Virtuoso library with by amending `@RunWith(IntegratedCucumber.class)`. Other classes should then extend either the BaseTest or SeleniumTest as described above.

Feature files can still be used to load test data, those that are set as "Examples" in the feature files trigger multiple test executions with each being reported as a separate test case in the Virtuoso logs. It is also possible to combine this with the Virtuoso test data functionality, for example this could be used for common Applicaiton-wide test data that does not related to specific features files.

For example, the runner could look like

```java
import org.junit.runner.RunWith;
import uk.gov.homeoffice.virtuoso.framework.bdd.IntegratedCucumber;
import cucumber.api.CucumberOptions;

@RunWith(IntegratedCucumber.class)
@CucumberOptions(format={"pretty",
    "html:target/test-report",
    "json:target/test-report.json",
    "junit:target/test-report.xml"},
    glue="calculator",
    features={"src/test/resources/features/Calculations.feature",
              "src/test/resources/features/CalculationSeries.feature"})
```

## Test Data
See [test data](/virtuoso/usage.md#test-data)

## Test Reports
See [test reports](/virtuoso/usage.md#test-reports)
