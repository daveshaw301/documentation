# Defining Parameters

A test execution via Virtuoso can be parameterised by configuring the parameters of the test. This can happen in three ways:

- A “config.properties” file that is part of the classpath and defines the parameters in a “key=value” way.
- A “config.xml” file that is part of the classpath and defines the parameters in a “<key>value</key>” way.
- Environment properties passed to the execution of the test suite, as “-Dkey=value”.

# Parameters
- [Test Data](#test-data)
- [Report Generation](#report-generation)
- [Test Execution](#test-execution)
- [SCP and SSH](#scp-and-ssh)
- [Connectivity](#connectivity)
- [Screen Recording](#screen-recording)
- [Accessing Parameters in Test Classes](#accessing-parameters-in-test-classes)
- [Example - config.properties](#config-properties)

## Test Data
| Key | Description | Acceptable Values | Required | Default |
| --- | ----------- | ----------------- | -------- | ------- |
|Data.HierarchySeparator|The separator to interpose between the components of a keyword.||Mandatory|.|
|Data.Format|Specifies the details of input data. Current supported formats are XLS and XLSX.||Mandatory|xlsx|
|Data.Dir|The directory from which to pick up the input data.|Directory name.|Mandatory|src/test/resources/test-data|
|Data.Common|The additional data files that each test will load.||Optional||
Data.\<test name\>.Specific|Data specific to each test.|Test method names, i.e. methods annotated with @org.junit.Test|Optional||
|Data.InputWorksheet|The name of the worksheet containing the input values. If an XLS file is defined, then a non-empty worksheet that matches the value of this key needs to exist.||Mandatory|Input Values|
|Data.ExpectedWorksheet|The name of the worksheet containing the expected results. If an XLS file is defined, then a non-empty worksheet that matches the value of this key needs to exist.||Mandatory|Expected Results|
|Data.Orientation|Determine whether data will be read in rows (first row keys, next rows values) or columns (first column keys).|Row or Column|Optional|Row|


## Report Generation
| Key | Description | Acceptable Values | Required | Default |
| --- | ----------- | ----------------- | -------- | ------- |
|Results.Dir|Specifies the relative path to the results directory.||Mandatory|test-results|
|Results.Format|Format to generate the report. Multiple formats can be selected using a comma as a separator, e.g. txt,xml.|txt, xml, xls|Mandatory||
|Results.File.Prefix|If present, the prefix will be used in constructing result file names, including the report|A string with characters that are valid in file names.|Optional||
|Results.File.Ext.Separator|If present, this defines the separator for the various components of the test report filename|A string with characters that are valid in file names.|Optional|.|
|Results.File.Timestamp|This boolean property determines whether to append the test start timestamp to result file names|true or false|Optional|false|
|Report.DateFormat|The format for the date to be used in the report.|Standard Java SimpleDateFormat input Strings.|Optional|yyyy.MM.dd HH: mm:ss|


## Test Execution
### Selenium Web Driver
| Key | Description | Acceptable Values | Required | Default |
| --- | ----------- | ----------------- | -------- | ------- |
|WebBrowser|Instructs Selenium as to which browser to use to drive the web pages.|Firefox, Chrome, IE, HTMLUnit, Saucelabs, SeleniumGrid.|Mandatory (for SeleniumTest)|Firefox|
|SeleniumGrid.URL|Specify the full URL of the Selenium Grid instance to connect to.|http://localhost:4444/wd/hub|Optional||
|WebBrowser.Profile|Tells Selenium to load a custom created profile for the browser. Currently supports Firefox only.|The name of your profile|Optional||
|Tracing|Set to no_log to remove tracing steps from the generated report.|no_log|Optional|Full tracing is enabled|
|Timeout.Explicit|Timeout to explicitly wait for.|Integer|Optional|120|
|Timeout.Implicit|Timeout to implicitly wait for when attempting to perform an action via Selenium.|Integer|Optional|5|
|Screenshots|Instructs Selenium whether to take screenshots upon test failure.|true or false|???|true|
|Screenshots.Dir|Instructs Selenium where to store screenshots.||???|test-results/screenshots||
|Screenshots.Scroll|Instructs Selenium to scroll by X pixels and take a second screenshot on error. Provide a positive value to scroll down, or a negative value to scroll up|250,-250,0,1000, etc|Optional||

### SauceLabs
| Key | Description | Acceptable Values | Required | Default |
| --- | ----------- | ----------------- | -------- | ------- |
|SauceLabs.Username|Username for SauceLabs account to execute tests from the cloud.||Mandatory (if WebDriver = SauceLabs)||
|SauceLabs.Password|Password for the SauceLabs username set to execute tests from the cloud.||Mandatory (if WebDriver = SauceLabs)||
|SauceLabs.Path|Path to SauceLabs account to execute tests from the cloud.||Mandatory (if WebDriver = SauceLabs)||

### SCP and SSH
| Key | Description | Acceptable Values | Required | Default |
| --- | ----------- | ----------------- | -------- | ------- |
|SCP.Executable|The executable to perform scp connections.|The path to the executable.|Optional||
|SCP.IdentityFile|A pem file that can be used for public-key authentication during ssh connections done for scp-ing.|The path to the pem file.|Optional||
SCP.User|The user account to scp to other hosts. The same (test) account should exist on all hosts.|The user account.|Optional||
|SSH.Executable|The executable to perform ssh connections.|The path to the executable.|Optional||
|SSH.IdentityFile|A pem file that can be used for public-key authentication during ssh connections.|The path to the pem file.|Optional||
|SSH.User|The user account to ssh to other hosts. The same (test) account should exist on all hosts.|The user account.|Optional||

## Connectivity ##
| Key | Description | Acceptable Values | Required | Default |
| --- | ----------- | ----------------- | -------- | ------- |
|Connectivity.Timeout|The timeout in routing to a remote host.||Optional||
|Connectivity.ifconfig|The executable to the ifconfig program.||Optional||
|Connectivity.route|The executable to the route program.||Optional||

## Screen Recording ##
| Key | Description | Acceptable Values | Required | Default |
| --- | ----------- | ----------------- | -------- | ------- |
|ScreenRecording|Enable or disable screen recording.|true or false|Optional|false|
|ScreenRecording.Quality|Video capture quality in frames per second (fps): 1 min at 30fps ~8.5MB, 1 min at 100fps ~11MB|Integer denoting frames per second.|Optional|30 (if ScreenRecording=true)|
|ScreenRecording.Dir|Specifies the output directory of the videos.|Directory name.|Optional|test-results/videos (if ScreenRecording=true)|

# Accessing Parameters in Test Classes
A test class can access the ExecutionContext singleton to read configData, as `ExecutionContext.getInstance().getConfigData().getProperty(“SSH”, “User”);`or
`ExecutionContext.getInstance().getConfigData().getIntProperty(“Timeout”, “Implicit”);`or
`ExecutionContext.getInstance().getConfigData().getBooleanProperty(“Screenshots”);`

However, test classes developed with Virtuoso are not generally expected to interact with the configuration data, which are used by the Virtuoso base classes and its execution engine.

## config.properties Template

Template config.properties file with default settings

```
################################################################################
## Test Data                                                                  ##
################################################################################
# The separator to interpose between the components of a keyword.
Data.HierarchySeparator=.

# Specifies the details of input data.
# Current supported formats are XLS and XLSX.
Data.Format=xlsx

# The directory from which to pick up the input data.
Data.Dir=src/test/resources/test-data

# The additional data files that each test will load. [optional]
#Data.Common=

# Data specific to each test method [optional]
#Data.<test name>.Specific=

# The name of the worksheet containing the input values.
Data.InputWorksheet=Input Values

# The name of the worksheet containing the expected results
Data.ExpectedWorksheet=Expected Results

# Determine whether data will be rad in rows or columns [optional]
#Data.Orientation=Row

################################################################################
## Report Generation                                                          ##
################################################################################
# Specifies the output format and directory of results.
Results.Dir=test-results

# Current supported formats are TXT, XML and XLS
Results.Format=txt

# The prefix will be used in constructing result file names, including the
# report, if present [optional]
#Results.File.Prefix=TestResultFile

# This defines the separator for the various components of the test report
# filename [optional]
#Results.File.Ext.Separator=.

# This boolean property determines whether to append the test start timestamp
# to result file names [optional]
#Results.File.Timestamp=true

# The format for the date to be used in the report.
# Uses standard Java SimpleDateFormat input Strings.
# Default is: yyyy.MM.dd HH:mm:ss
#Report.DateFormat=yyyy.MM.dd

################################################################################
## Web Driver                                                                 ##
################################################################################
# Instructs Selenium whether to take screenshots and where to store them.
Screenshots=true
Screenshots.Dir=test-results/screenshots
# Instructs Selenium as to which browser to use to drive the web pages.
# Current working values are:
# - Firefox
# - Chrome
# - IE
# The default value is Firefox.
WebBrowser=Firefox

# Set to no_log to remove tracing steps from the generated report.
#Tracing=no_log

# Timeout to explicitly wait for [optional]
#Timeout.Explicit=120

# Timeout to implicitly wait for when attempting to perform an action
# via Selenium [optional]
#Timeout.Implicit=5

# Instructs Selenium whether to take screenshots upon test failure
Screenshots=true

# Instructs Selenium where to store screenshots
Screenshots.Dir=test-results/screenshots

################################################################################
## SauceLabs                                                                  ##
################################################################################

# Username for SauceLabs account to execute tests from the cloud
#SauceLabs.Username=

# Password for the SauceLabs username set to execute tests from the cloud
#SauceLabs.Password=

# Path to the SauceLabs account to execute test from the cloud
#SauceLabs.Path=

################################################################################
## SCP and SSH                                                                ##
################################################################################

# The executable to perform scp connections [optional]
#SCP.Executable=scp

# A pem file that can be used for public-key authentication during SCP
# connections [optional]
#SCP.IdentityFile=

# The user account to SCP to other hosts. The same (test) account should exist
# for all hosts [optional]
#SCP.User=

# The executable to perform ssh connections [optional]
#SSH.Executable=ssh

# A pem file that can be used for public-key authentication during SSH
# connections [optional]
#SSH.IdentityFile=

# The user account to SSH to other hosts. The same (test) account should exist
# for all hosts [optional]
#SSH.User

################################################################################
## Connectivity                                                               ##
################################################################################

# The timeout in routing to a remote host [optional]
#Connectivity.Timeout=5

# The executable to the ifconfig program [optional]
#Connectivity.ifconfig=/sbin/ifconfig

# The executable to the route program [optional]
#Connectivity.route=/sbin/route

################################################################################
## Screen Recording                                                           ##
################################################################################

# Enable or disable screen recording [optional]
#ScreenRecording=true

# Video capture quality in frames per second (fps) [optional]:
# 1min at 30fps ~ 8.5MB, in at 100fps ~ 11MB
#ScreenRecording.Quality=30

# Specifies the output director of the videos [optional]
#ScreenRecording.Dir=test-results/video
```

