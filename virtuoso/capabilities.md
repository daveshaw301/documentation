The current version of the Virtuoso library is 1.4. The supported capabilities are
## Virtuoso 1.4 ##
- Maven 3.0 support
- Integration with Cucumber JVM
- Parameterisation set as environment properties
- Log4j support at the framework level
- Video capturing feature (configurable)

## Virtuoso 1.35 ##
- Web browser testing via Selenium
- Weblogic Broker testing
- Connectivity testing between the hosts of the SUT
- Oracle Database testing support
- SOAP service testing
- JMS queue testing support

For specific changes see the [changelog](https://gitlab.com/tdcs_framework/virtuoso/blob/master/CHANGELOG)
