In order for a test suite to use Virtuoso it needs:
- A version of the Virtuoso jar, which should be included as a dependency to the project.

Versions of the Virtuoso library before 1.4 bundle all dependent libraries within the Virtuoso jar file. This means that a test suite only needs to add this bundled Virtuoso library to its classpath for all its dependencies to be resolved.

## Maven

From version 1.4 onwards the dependencies of Virtuoso are resolved via the transitive dependency resolution mechanism of Maven. Assuming that the Virtuoso library is installed in a Maven repository, then any project that has access to that repository and adds Virtuoso as a dependency to its Maven file will automatically fetch any additional dependent libraries that are needed by Virtuoso itself.

For example to add Virtuoso 1.4 as a dependency requires the following to be added to the pom.xml
```
<dependencies>
    <dependency>
        <groupId>uk.gov.homeoffice</groupId>
        <artifactId>virtuoso</artifactId>
        <version>1.4</version>
    </dependency>
</dependencies>
```

## Non-Maven Projects

Non-mavenized projects that use Virtuoso version 1.4 and higher will need to explicitly add to their classpaths the Virtuoso library and all its dependent libraries as they are defined in its pom.xml.

## Configuration
- To ensure that its execution context includes the mandatory configuration parameters, as listed in [Configuration](/virtuoso/configuration.md). This can either be done by adding a configuration properties file to a directory that is included in the classpath, or by defining the mandatory properties as environmental variables. The values that are set for paths, executables, and filenames should be valid.

## Base Class
- The test classes should subclass one of the Virtuoso base test classes offered by the framework. These are:

| Base class | Type of test | Virtuoso Version |
| :--------- | :----------- | :--------------: |
|BaseTest|Any type of test that does not fall under any of the more specific categories below.|1.0|
|SeleniumTest|Tests that use the Selenium Web Driver to perform UI automated testing. The Selenium Web Driver is spawned before the execution of each of test method of a class that extends the SeleniumTest base class.|1.0|

- The page objects used in Selenium tests should extend the BasePage class of Virtuoso and define PageElement objects. This allows for the automated tracing and reporting at the back of the page actions.
- The host objects for Connectivity tests should extend the Host class of Virtuoso.

## Assertor
The default assertions from JUnit should use the “assertor” object which is inherited by the Virtuoso base test classes.
