This tutorial presents how to script a hypothetical UI-based test using Virtuoso and its Selenium capabilities. Assuming the following test steps:

| ID | Test Steps | Expected Result |
| --- | ---------- | --------------- |
|1|Go to eBay home page|eBay home page is displayed|
|2|Enter a search term|Autocomplete options are shown below search bar|
|3|Click first autocomplete option|Search results page is displayed with items matching the search term|

The test suite will have to define a test method that implements this script. 

## Test Development ##
As described in the Usage section, the test will be developed in three steps:

1. Project initialisation (Steps 1 – 4)
2. Test data and page object classes (Steps 5 – 7)
3. Test classes (Steps 8 – 10)

This tutorial will cover the steps in developing each of these in detail. The tutorial uses Eclipse with the maven plugin M2Eclipse.

### Step 1: Create a new Java project ###
The picture below shows the structure of a new maven project created in Eclipse.

![image](virtuoso/images/tutorial-step1.png)

The `src/main/java` directory will include the classes that provide an abstraction to the system under test. In this case, these will be related to the page objects that comprise the application.

The `src/test/java` directory will define the test class that implements the test script logic, using the page abstractions of the `src/main/java` classes.

### Step 2: Add project dependencies ###
The next step is to add the Virtuoso jar as a dependency in the project's pom file: 

```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>uk.gov.homeoffice</groupId>
    <artifactId>virtuoso-ebay-sample</artifactId>
    <version>1.0</version>

    <dependencies>
        <dependency>
            <groupId>uk.gov.homeoffice</groupId>
            <artifactId>virtuoso</artifactId>
            <version>1.4</version>
        </dependency>
    </dependencies>
</project>
```

### Step 3: Add a properties file ###
Now to add a file where configurable properties can be specified. Add a file called `config.properties` in the `src/test/resources` directory.


![EBayTest Properties File](virtuoso/images/tutorial-step3.png)

### Step 4: Set the parameters of the config.properties file ###
See below for the minimum properties that need to be defined in the config.properties file for this tutorial.

```
################################################################################
## Test Data                                                                  ##
################################################################################
Data.HierarchySeparator=.
Data.Format=xlsx
Data.Dir=src/test/resources/test-data
Data.InputWorksheet=Input Values
Data.ExpectedWorksheet=Expected Results
Data.Common=

################################################################################
## Report Generation                                                          ##
################################################################################
Results.Dir=test-results
Results.Format=txt

################################################################################
## Web Driver                                                                 ##
################################################################################

WebBrowser=Firefox
Screenshots=true
Screenshots.Dir=test-results/screenshots
```
*Data.Common= should not be required as is an optional component. This will be fixed in the release after 1.4.0.1*

### Step 5: Create test data directory ###
Create a folder called `test-data` in the `src/test/resources` directory. Then create a spreadsheet  `EbayTest` and copy it to the `src/test/resources/test-datà` directory. 

![EbayTest Spreadsheet Image](virtuoso/images/tutorial-step5.png)


### Step 6: Create a page objects class ###
Create a package called `ebay` in the `src/main/java` directory and then create a class named `EbayHomePage`. This will be the first page objects class.

![Ebay package image](virtuoso/images/tutorial-step6.png)


### Step 7: Write the first page objects class ##
Each page objects class must extend the Virtuoso BasePage class. Then it is on to defining the page objects of eBay"s home page. To do this, navigate to www.ebay.com and locate the necessary page objects needed for the tests being written but also with the thought of which page objects might be necessary for future tests. Tools such as Firebug (version 2.0.9.1) and FirePath (version 0.9.7.1) for the Firefox web browser can be useful when finding the best location paths of an html element. In Chrome and Internet Explorer press F12 to get similar capabilities using the developer tools.

The code below shows the page objects needed for this tutorial.

```java
package ebay;

import uk.gov.homeoffice.virtuoso.framework.ExecutionContext;
import uk.gov.homeoffice.virtuoso.framework.element.PageElement;
import uk.gov.homeoffice.virtuoso.framework.page.BasePage;


public class EbayHomePage extends BasePage {
  
  private static final PageElement SEARCH_BAR = PageElement.byId("gh-ac", "Home Page search bar");
  private static final PageElement AUTO_COMPLETE_FIRST_OPTION = PageElement.byCssSelector(
      ".ghAC_hl", "Autocomplete menu first option");

}
```

After the page objects have been defined, the services by which these objects will be interacted with in the tests must be created. As can be seen in the test method already written, services that are needed include launching the home page, entering a search term etc.

```java
  /**
   * Launch eBay home page
   * 
   * @param url of eBay home page
   * @return EbayHomePage
   */
  public static EbayHomePage launch(String url) {
    ExecutionContext.getInstance().getPilot().navigateTo(url);
    return new EbayHomePage();
  }

  /**
   * Enter item as search term into the search bar.
   * 
   * @param item to search for.
   */
  public void enterSearchTerm(String item) {
    pilot.sendKeys(SEARCH_BAR, item);
  }
  
  /**
   * Click search bar.
   */
  public void clickSearchBar() {
    pilot.click(SEARCH_BAR);
  }
  
  /**
   * Get the text of autoacomplete"s first option.
   * 
   * @return text of autocomplete"s first option.
   */
  public String getAutoCompleteFirstOptionText() {
    return pilot.getText(AUTO_COMPLETE_FIRST_OPTION);
  }
  
  /**
   * Select the first option from autocomplete drop down.
   * 
   * @return EbayItemSearchPage
   */
  public EbayItemSearchPage selectFirstOptionFromAutoCompleteDropDown() {
    pilot.click(AUTO_COMPLETE_FIRST_OPTION);
    return new EbayItemSearchPage();
  }
```

The `selectFirstOptionFromAutoCompleteDropDown` method demonstrates a key principle when writing services to interact with page objects. If calling a service will cause the webpage to change then that service method should return an object of the page which is being navigated to. To resolve the compilation errors another page object class must be created called `EbayItemSearchPage`. Create that the same way as when the `EbayHomePage` class was created, including the extension of the Virtuoso `BasePage` class.

The code below shows the EbayItemSearchPage class.

```java
package ebay;

import uk.gov.homeoffice.virtuoso.framework.page.BasePage;

public class EbayItemSearchPage extends BasePage {
  
}
```

### Step 8: Add data to test-data file ###
Open up the test data spreadsheet in the directory `/src/test/resources/test-data` Below are the keys and values required in the **Input Values** tab.

| HomePage.Url | HomePage.SearchItemText |
| ------------ | ----------------------- |
|http://www.ebay.co.uk/|macbook|

Below are the keys and values required in the **Expected Results** tab.

| HomePage.Title | Search.AutoComplete.FirstOption | Search.FirstOption.Title |
| -------------- | -------------------------------- | ------------------------ |
| Electronics, Cars, Fashion, Collectibles, Coupons and More &#124; eBay | macbook | macbook &#124; eBay | 

### Step 9: Create a test class ###
Create a package and name it `tests` in the `src/test/java` directory and then create a new class called `EbayTest`. This is the class where the first test will be written.

![EbayTest class image](virtuoso/images/tutorial-step9.png)

### Step 10: Write the first test ###
Open `EbayTest.java` which was just created and import the SeleniumTest from the framework and the classes you created. Extend the Virtuoso `SeleniumTest` class to begin writing the test method. 

```java
package tests;

import org.junit.Test;

import uk.gov.homeoffice.virtuoso.framework.test.SeleniumTest;
import ebay.EbayHomePage;
import ebay.EbayItemSearchPage;

public class EbayTest extends SeleniumTest {

  @Test
  public void itemSearchAutoComplete() {

  }

}
```

This test should enter a search term into the search bar on the eBay home page and select the first autocomplete option. Each test step takes place by accessing one of the methods of the page objects that were defined in Steps 6 and 7. Expected results use the “assertor” object to log any checks that were performed as part of the test. The test class is easily read in English. Any action like “selectFirstOptionFromAutoCompleteDropDown“ that leads to a new page is implemented via a method that returns a new object of that page. 

The snippet below also shows how to invoke test data and shows the differentiation in the invocation of input values and expected results.

```java
package tests;

import org.junit.Test;

import uk.gov.homeoffice.virtuoso.framework.test.SeleniumTest;

public class EbayTest extends SeleniumTest {

  @Test
  public void itemSearchAutoComplete() {
    
    // Test Step 1
    EbayHomePage ebayHomePage = EbayHomePage.launch(testData.getInputValue("HomePage", "Url"));
    
    // Expected Result 1
    assertor.assertEquals("eBay home page verification",
        testData.getExpectedResult("HomePage", "Title"), ebayHomePage.getTitle());
    
    // Test Step 2
    ebayHomePage.clickSearchBar();
    ebayHomePage.enterSearchTerm(testData.getInputValue("HomePage", "SearchItemText"));
    
    // Expected Result 2
    assertor.assertEquals("Autocomplete first option text",
        testData.getExpectedResult("Search", "AutoComplete", "FirstOption"),
        ebayHomePage.getAutoCompleteFirstOptionText());
    
    // Test Step 3
    EbayItemSearchPage ebayItemSearchPage =
        ebayHomePage.selectFirstOptionFromAutoCompleteDropDown();
    
    // Expected Result 3
    assertor.assertEquals("First autocomplete option title verification",
            testData.getExpectedResult("Search", "FirstOption", "Title"),
            ebayItemSearchPage.getTitle());;
  }

}
```

### Step 11: Run the test and review results ###
To run the test in Eclipse, right-click the test class in the package explorer and choose Run As → Junit Test. The results of the test run will be stored in the directory test-results. If the test results do not appear immediately then refresh the project.

To run the test using maven on the command line, run the command `mvn test` at the project's root directory. The results of this test run will be printed inside the terminal window but can also be reviewed in more detail in the test-results folder inside the project root directory.

The test report generated as a text file, due to the `Results.Format=txt` parameter in the config.properties file, shows in plain English the exact steps that were followed during the script execution:

```
================================================================================
Test:     itemSearchAutoComplete
Status:   PASS
Started:  2015.05.25 15:29:32
Ended:    2015.05.25 15:29:44
Trace:    10 step(s)
   1) [2015.05.26 15:29:39]: ui action: navigate to(http://www.ebay.co.uk/)
   2) [2015.05.26 15:29:39]: ui action: get title(Electronics, Cars, Fashion, Collectibles, Coupons and More | eBay)
   3) [2015.05.26 15:29:39]: assert: eBay home page verification(pass)
   4) [2015.05.26 15:29:39]: ui action: click(Home Page search bar)
   5) [2015.05.26 15:29:40]: ui action: set text(Home Page search bar,macbook)
   6) [2015.05.26 15:29:40]: ui action: get text(Autocomplete menu first option,macbook)
   7) [2015.05.26 15:29:40]: assert: Autocomplete first option text(pass)
   8) [2015.05.26 15:29:43]: ui action: click(Autocomplete menu first option)
   9) [2015.05.26 15:29:44]: ui action: get title(macbook | eBay)
  10) [2015.05.26 15:29:44]: assert: First autocomplete option title verification(pass)
```

Steps 1 – 5 need to be implemented once for the test suite. Steps 6 and 7 depend on the complexity of the system under test and the number of pages that exist in it. Typically, an implementation overhead is spent at the early stages of the test automation suite development for page object classes development and subsequently the methods of these classes  are invoked by various test classes.