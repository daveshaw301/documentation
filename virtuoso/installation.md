Virtuoso can be deployed by either building locally or referencing the maven repository.

# Download and Build locally


To install Virtuoso locally, follow the steps below. Check that you have met the [pre-requisites.](https://gitlab.com/tdcs_framework/documentation/wikis/pre-requisites)

## Download 

You can either  
1. Download a tar or zip from - https://gitlab.com/tdcs_framework/virtuoso/tags
2. Clone the repository from a terminal `git clone --depth 1 git@gitlab.com:tdcs_framework/virtuoso.git`

*You can download the complete repository if necessary without the `--depth 1`, however when you go back to a depth before v1.4
the repository will be over 160mb as Virtuoso was not mavenized so included all libraries referenced*

## Build
The installation of Virtuoso comprises the building of the library.

### Virtuoso 1.4 and above 
Use Maven to build with `mvn clean install`

This will
- download all dependencies for Virtuoso
- generate the Virtuoso jar in `dist/` directory
- smoke test the build
- install the package in your local repository `/.m2/repository/uk/gov/homeoffice/virtuoso/1.4/virtuoso-1.4.jar`

If the build fails (for instance Firefox errors), you can retest with `mvn clean test`. If you need to compile the build again then `mvn clean package` will build locally in the `/dist` folder and you can move the package to the local repository with `mvn install:install-file -Dfile=dist/virtuoso-<version>.jar -DpomFile=pom.xml`

### Before Virtuoso 1.4
Use `ant dist` to generate the Virtuoso jar under the `dist/` directory with the version number defined in the build.xml file at the root directory of the source code.

# Maven Repository
From Virtuoso 1.4.2 onwards, Virtuoso is also hosted in a maven repository

## pom.xml
It can be included in any maven project, by including the following dependencies and repositories within the pom.xml
```
    <dependencies>
        <!-- Virtuoso dependencies -->
        <dependency>
            <groupId>uk.gov.homeoffice</groupId>
            <artifactId>virtuoso</artifactId>
            <version>1.4.2</version>
        </dependency>
        <dependency>
            <groupId>uk.gov.homeoffice</groupId>
            <artifactId>virtuoso</artifactId>
            <version>1.4.2</version>
            <classifier>javadoc</classifier>
        </dependency>
        <dependency>
            <groupId>uk.gov.homeoffice</groupId>
            <artifactId>virtuoso</artifactId>
            <version>1.4.2</version>
            <classifier>sources</classifier>
        </dependency>
    </dependencies>
    <!-- Maven Repository on GitLab for TDCS Framework -->
    <repositories>
        <repository>
            <id>tdcs_framework-repository</id>
            <url>https://gitlab.com/tdcs_framework/maven/raw/master/repository/</url>
        </repository>
    </repositories>
```
## Versions
Check https://gitlab.com/tdcs_framework/maven to see versions available within the repository and amend the <version> tag as necessary.

