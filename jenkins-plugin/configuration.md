## Vitraux-Virtuoso (Automation)
A job that runs as part of its workflow a test automation suite developed with the Virtuoso library needs to add a 'Run Virtuoso/Maestro test suite' as a build step.

A sample configuration is shown in the picture below:

![jenkins-virtuoso](jenkins-plugin/images/jenkins-virtuoso.png)

- The **Test Type** should be selected to be `Automation (Virtuoso)`.
- The **Server** should be a valid Vitraux server. When the server and port are set in this field, the plugin attempts to send an acknowledgement message to the Vitraux server to confirm its presence. If it does not get a response, then an error message will appear below the field. This is a useful way to confirm that the Vitraux server is up and running from within the plugin.
- The **Test Suite** is the zip file that contains all required files to run the test in Virtuoso. For Vitraux Server v1.4 onwards this is a test automation suite based on Maven 3.0, containing the source code, the test code and the pom.xml. The pom.xml needs to be at the root of the zip file. The default location for test data within the suite zip is directory `src/test/resources/test-data/` within the zip. Test data can also be defined externally from the zip, using the 'Test Data' parameter.
- The **Test Name** is typically the test suite class to be executed. However, the defined value is in fact the 'test' argument to Maven's surefire plugin, i.e. the Vitraux server will execute `mvn clean -q test -Dtest=<value of Test Name>`. It is thus possible to use wildcards to execute sets of tests as is typically possible with the Maven surefire plugin. 
- The **Environment File** can be used to define key-value pairs that might be environment-specific, e.g. service endpoints, different environment URLs, etc. and need to be defined dynamically. This allows for the same test suite to be executed against different environments. Values that also exist in the config.properties are overridden by the ones of the environment file. The screenshot above shows a typical use case, where the name of the environment file is dynamically selected using a Jenkins parameter or a 'Parameterised build' (see Usage - Parameter Replacement). 
- The **Test Timeout** can be used to set a maximum duration to the execution of a test in minutes. If not specified, then the job will wait until the test execution on the Vitraux side is completed (or manually killed).
- The **Server Proxy** can be used in the case that the Vitraux server is hidden behind a proxy server. If the server proxy is specified, then the 'Server'will need to be a hostname that is valid at the proxy side.
 
## Vitraux-Maestro (Performance)
A job that runs as part of its workflow a performance test developed for the Maestro tool needs to add a 'Run Virtuoso/Maestro test suite' as a build step.

A sample configuration is shown in the picture below:

![jenkins-maestro](jenkins-plugin/images/jenkins-maestro.png)

- The **Test Type** should be selected to be “Performance (Maestro)”.
- The **Server** should be a valid Vitraux server. When the server and port are set in this field, the plugin attempts to send an acknowledgement message to the Vitraux server to confirm its presence. If it does not get a response, then an error message will appear below the field. This is a useful way to confirm that the Vitraux server is up and running from within the plugin.
- The **Test Suite** is the zip file that contains all required files to run the test in Maestro. A Maestro-plan XML file needs to be present at the root directory of this zip file.
- The **Test Name** is a string that, together with the Jenkins job name, is used to uniquely identify a test.
- The **Environment File** can be used to define key-value pairs that might be environment specific, e.g. service endpoints, different environment URLs, etc. and need to be defined dynamically. This allows for the same test suite to be executed against different environments. Values that also exist in the config.properties are overridden by the ones of the environment file. The screenshot above shows a typical use case, where the name of the environment file is dynamically selected using a Jenkins parameter or a “Parameterised build” (see Usage - Parameter Replacement).
- The **Test Timeout** can be used to set a maximum duration to the execution of a test in minutes. If not specified, then the job will wait until the test execution on the Vitraux side is completed (or manually killed).
- The **Server Proxy** can be used in the case that the Vitraux server is hidden behind a proxy server. If the server proxy is specified, then the 'Server' will need to be a hostname that is valid at the proxy side.

The help icons next to each field can be used for quick reference of the meaning from each field.

