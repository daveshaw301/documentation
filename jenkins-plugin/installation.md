## Download

Download from - https://gitlab.com/tdcs_framework/jenkins-plugin

## Build
To create the HPI file (apt-plugin.hpi) use `mvn clean install -DskipTests`

## Jenkins Installation
The installation of the Vitraux plugin comprises the installation of the hpi file. Assuming adequate permissions at the Jenkins instance:

1. Go to “Manage Jenkins”
2. Go to “Manage Plugins”
3. Go to the “Advanced” tab
4. Under the “Upload Plugin” section, choose the apt-plugin.hpi file.
5. Click “Upload”

The generated directory under the “plugins” directory should be called “apt-plugin”. It should not be changed.

Updating a previous version of the plugin is typically done by uninstalling the previous version and installing the next one as listed above.