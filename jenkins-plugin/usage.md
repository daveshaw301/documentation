## Parameter Replacement
Jenkins Jobs can be parameterised to define values that dynamically configure a test execution with Vitraux. A build should be set as a **Parameterised Build** and the parameter name **key** and its type or list of acceptable values should be defined. Users can then set the value of this parameter when they run a new build for the job. Every occurrence of the ${key} string in any of the plugin's fields will then be replaced by the set values.

The typical use case for this feature relates to test environment selection. For example, a parameter **ENVIRONMENT** could be defined with acceptable values **dev**, **uat** and **prod**. The `Environment File` field can then be set as `conf/${ENVIRONMENT}.properties`, so that the environment file is selected based on the Jenkins build parameter. Crucially, the three possible environment files, `dev.properties`, `uat.properties`, and `prod.properties` should already exist under the `conf` directory for this approach to work for all possible parameter selections.

From v1.13 onwards, multiple environment files can be defined as a list of comma-separated files, i.e. `a.csv,b.props,c.properties` etc. (no spaces between files). If any key values are common then the values encountered in the rightmost files will replace the ones encountered before. For example, in the previous list, `a.csv,b.props,c.properties`, if the files had the following values 

|**File**|**Key**|**Value**|
| ------ | ----- | ------- |
|a.csv|Key1|1|
|a.csv|Key2|21|
|b.props|Key2|22|
|b.props|Key3|3|
|c.properties|Key4|4|

The value that will be used for `Key2` will be `22`.

Jenkins parameters from Jenkins parameterised builds can also be used to define dynamic parameters. They supersede any previously defined values, i.e. if the Jenkins build is parameterised with Key2, then the value selected for this parameter within Jenkins will be the one used in the automation or performance test.

Parameters could also be used for timeout values, with different possible maximum durations for each type of test. 

## Running a Job
Once a Job is configured to use the Jenkins plugin, then its build can be triggered, either automatically or at the back of a Jenkins action. Once any parameter values are selected, the Jenkins job starts getting executed. When it reached the “Run Virtuoso/Maestro build step”, then it will perform a series of actions which can be monitored during the execution at the “Console Output” of the job. Specifically, the plugin will:

1. Send the zip file of the “Test Suite” to the Vitraux server to register the test. Any previous version of the test, identified by the Jenkins job name and the “Test Name” will be fully overwritten. A confirmation message that the registration was completed successfully should be shown.
2. Trigger the execution of a registered test. The Vitraux server will search its list of registered tests and assuming it finds one that matches the Jenkins job name and “Test Name” combination, it will start executing it. 
3. The plugin code will be polling the Vitraux server to investigate the state of a job. A job can be queued, running, or completed. If a test is running, then anything that is output from the test code to the System.out is transferred and rendered in the Console Output screen of the Jenkins job. This allows for live monitoring of a running test.
4. If the polling request finds that the test has completed its execution, then it will attempt to fetch the test results (as a zip) from the Vitraux server, extract them locally, and generate the report for them.
5. When the test job has been successfully finished (not ABORTED) then a Virtuoso or Maestro report will be generated for that build. Visiting the build and clicking on the corresponding report link, will show the results of the run. 

## Build Report
Virtuoso Reports show a list of links to XML files that contain the results of the test run. Clicking on each of the links toggles a table that shows all tests that have been executed together with the outcome of the execution. The traced steps during the test execution can be viewed by clicking on the class name of each tests. Traced steps typically include UI actions and assertions.

![jenkins-virtuosoreport](jenkins-plugin/images/jenkins-virtuosoreport.png)

A link to a zip that includes all test automation results (i.e. XML files and screenshots) is shown at the end of the Virtuoso Report page for a build.

For a Maestro test, the report shows all graphs and tables that were generated at the back of the performance test, as per the Maestro plan. A link to the zip file with all generated results can be found at the bottom of the report page.

![jenkins-maestroreport](jenkins-plugin/images/jenkins-maestroreport.png)

## Troubleshooting
1. Is the Vitraux server up and running?
2. Can the Jenkins instance access the Vitraux server? Are there any firewall restrictions or connectivity issues?
3. Are the "Configurations" of the test job correct? Are there any error messages reported under the corresponding Virtuoso/Maestro fields?
4. Does the zip “Test Suite” file have a valid structure to be executed by Virtuoso or Maestro?
5. Are there any errors on the “Console Output” of the job builds?
6. Has the results zip been created under the “results/” location at the Vitraux server?
7. Has the results zip been successfully copied over from the Vitraux server and extracted to Jenkins instance filesystem (a 404 response might be returned in place of a response if this is not the case)? 
8. Are all the Requirements met?

## Multiple Test Composition
More than one “Run Virtuoso/Maestro test suite” build steps can be defined in the same Jenkins job. This feature can be used to execute test classes in different jar (and corresponding zip) files or in order to generate reports for each test suite separately. The same thing could also be achieved with different Jenkins jobs, but in cases it might be desirable for a single job to perform all testing. Combinations of Virtuoso and Maestro tests are also possible in this way.

