The installation of Vitraux comprises of:
- setting up of a Linux server
- installing all required external tools to this Linux server and configure the filesystem
- building and deploying servlet wars for serving Virtuoso and Maestro tests
- starting the Tomcat server

The following are for Vitraux 1.4 onwards.

# Set up a Linux server
A Vitraux server can act as a test automation server (Vitraux-Virtuoso), performance testing server (Vitraux-Maestro), or both (Vitraux). Vitraux is currently only supported for Linux as it makes use of external third-party tools for Linux.

## Hardware Specifications
The recommended specifications for these servers are:

**Vitraux-Virtuoso:**
- CentOS 6.5 or CentOS 7
- Dual-Core 64-bit
- 4GB RAM
- 60GB disk space

**Vitraux-Maestro:**
- CentOS 6.5 or CentOS 7
- Quad-Core 64-bit
- 8GB RAM
- 60GB disk space

The highest spec'd Vitraux-Maestro can be used, if Vitraux is to serve both types of tests. Deploying Vitraux on other distributions of Linux is possible, but has not been tested yet.

CentOS 7 allows the installation of the latest Google Chrome versions for automated tests, while CentOS 6.5 will only support Firefox

## Packages
The following packages are required for Vitraux. The steps below will outline how to install on a clean CentOS 6.5 and CentOS 7 installations. These may differ depending upon your specific infrastructure and should only be used as a guide

### Vitraux
- Java JDK 8 ([latest](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html))
- Apache Tomcat (v6/7/8 have been tested)
- MySQL server
- MySQL Connector/J (for CentOS 6.5)

### Virtuoso
- Apache Maven v3.0+
- Xvfb
- Firefox (recommend ESB release - check compatibility with Selenium version in Virtuoso deployed)
- Chrome (if required and using CentOS 7)
- Selenium Chromedriver (if using Chrome)
- Vitraux-Virtuoso servlet war

### Maestro
- Apache JMeter
- System Stats 10.1.5
- Gnu plot 4.6.4
- Vitraux-Maestro servlet war

## Firewall rules
**To accept requests:** The port where the Tomcat server listens for requests needs to be opened. By default, this is TCP port 8080. Traffic to that port is expected from any Jenkins server that has a Vitraux-plugin installed or from a proxy server between such a Jenkins server and Vitraux.

**To run tests:** Access from the Vitraux server to the system under test (SUT) is required. The specific ports that have to be opened depend on the services that are tested on the SUT.

**To monitor servers:** Port 22, used for ssh, needs to be opened to all servers that a Vitraux-Maestro server will be monitoring, during a performance test run. Database server ports of the SUT might also need to be opened to traffic from Vitraux if these servers are also monitored.

**To resolve dependencies:** For Vitraux-Virtuoso 1.4 onwards, access to an artefact repository (e.g Nexus, Artifactory, etc.) that is used by test engineers and which contains releases of the Virtuoso library is recommended. If no such repository exists, then public Maven repositories can be used in combination with local deployments of Virtuoso library releases on the Vitraux server.

**To perform maintenance:** If possible, the Vitraux server should allow outgoing traffic towards the Internet to perform maintenance tasks and resolve dependencies using public repositories. Inbound traffic to port 22 should be allowed.

## Test Runner Account
A new user account, "vitraux” needs to be created for that server. This account:
- will be used to execute the Tomcat server, instead of the default “tomcat” one
- will need to have access to all directories and executables that Vitraux
- will need to have password-less public-key authentication set up to all servers that have to be monitored in a Vitraux-Maestro execution
- if a local deployment of the Virtuoso library is used, then it will have to be deployed under /home/vitraux/.m2/repository/

# Installation
**The steps below are a guide for how to install Vitraux on a clean install of CentOS. There are different ways that these steps can be achieved and amendments may be required depending upon your environment and build. The steps also assume direct access to the internet and will need to be amended if you are using an artefact repository.**

# CentOS 6.5
This has been built against CentOS-6.5-x86_64-minimal.iso and CentOS-6.7-x86_64-minimal.iso

The installation was run with the user **vitraux** with sudo permissions

## Vitraux (Common)
### Deployment tools
If using the minimal installation of CentOS 6.5 then this does not include wget or some of the compression packages required
```
sudo yum install wget unzip zip -y
```

### Java Development Kit 8 (JDK)
The Java virtual machine and development kit to compile Java files to bytecode.
```
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u66-b17/jdk-8u66-linux-x64.rpm"
```

Install
```
sudo yum install jdk-8u66-linux-x64.rpm -y
```

### Apache Tomcat
To install
```
sudo yum install tomcat6.x86_64 -y
```
This will install Tomcat6 in `/usr/share/tomcat6`

#### Firewall Config
By default, Vitraux will require TCP port 8080 to be open on the firewall, check with the system admin on how to configure and if different ports should be used.

If you have permission, this will open TCP port 8080
```
sudo chkconfig iptables on
sudo iptables -I INPUT -p tcp --dport 8080 -j ACCEPT
sudo service iptables save
```

To check rules `sudo iptables -L -n`

#### Permissions
Add vitraux to the tomcat group `sudo usermod -a -G tomcat vitraux`
This can be confirmed with `id vitraux`

Amend the user and group in `/etc/sysconfig/tomcat6` so that the vitraux user will execute the tomcat process
```
sudo sed -r -i 's/#TOMCAT_USER="tomcat"/TOMCAT_USER="vitraux"/g' /etc/sysconfig/tomcat6
sudo sed -r -i 's/#TOMCAT_GROUP=/TOMCAT_GROUP=/g' /etc/sysconfig/tomcat6
```

Add the user "vitraux" to `/usr/share/tomcat6/conf/tomcat-users.xml`
```
sudo sed -r -i 's/<tomcat-users>/<tomcat-users>\n  <user username="vitraux" password="vitraux" roles="tomcat"\/>/g' /usr/share/tomcat6/conf/tomcat-users.xml
```

Make vitraux the owner for the following folders
```
sudo chown -R vitraux.vitraux /var/cache/tomcat6/
sudo chown -R vitraux.vitraux /usr/share/tomcat6/webapps/
sudo chown -R vitraux.vitraux /var/log/tomcat6/
sudo chown -R vitraux.vitraux /etc/tomcat6/
sudo chown -R vitraux.vitraux /etc/sysconfig/tomcat6
sudo chown -R vitraux.vitraux /etc/logrotate.d/tomcat6
```

#### Service
How to start/stop/restart the service and add to startup if required.

- Start `sudo service tomcat6 start`
- Stop `sudo service tomcat6 stop`
- Restart `sudo service tomcat6 restart`
- Enable on startup `sudo chkconfig --add tomcat6`

Start the service.

Check that the vitraux user is running the tomcat service and not the tomcat user with `ps aux | grep tomcat6`. The user is the first entry -
```
vitraux   9332  5.7  6.9 2193380 52668 ?       Sl   10:36   0:01 /usr/java/jdk1.8.0_66/bin/../bin/java -Djavax.sql.DataSource.Factory=org.apache.commons.dbcp.BasicDataSourceFactory -classpath :/usr/share/tomcat6/bin/bootstrap.jar:/usr/share/tomcat6/bin/tomcat-juli.jar:/usr/share/java/commons-daemon.jar -Dcatalina.base=/usr/share/tomcat6 -Dcatalina.home=/usr/share/tomcat6 -Djava.endorsed.dirs= -Djava.io.tmpdir=/var/cache/tomcat6/temp -Djava.util.logging.config.file=/usr/share/tomcat6/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager org.apache.catalina.startup.Bootstrap start
```

Enable the service on startup

### MySQL Server
Database server used to hold information on test assemblies and runs performed on this Vitraux server

The following will install MySQL Server and set the service to rn on startup. The database will be configured with

- User = `root`
- Password = `admin`

```
sudo yum install mysql-server.x86_64 -y
sudo chkconfig mysqld on
sudo service mysqld start
sudo mysqladmin -u root password admin
```

If you want to check the database run `mysql -u root -p` and enter the password. To show the databases available `show databases;` which should show 3 by default. To exit type `exit`

### MySQL Connector/J
JDBC driver for MySQL so the Vitraux servlet can connect to the database
```
wget http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.37.tar.gz
tar xvf mysql-connector-java-5.1.37.tar.gz
sudo cp mysql-connector-java-5.1.37/mysql-connector-java-5.1.37-bin.jar /usr/share/tomcat6/lib/
```

### Results Directory

Create the results directory in Tomcat webapps and give ownership to the vitraux user
```
sudo mkdir /usr/share/tomcat6/webapps/results && sudo chown -R vitraux.vitraux /usr/share/tomcat6/webapps/results
```

## Vitraux-Virtuoso
### Apache Maven 3.X
Build manager for Java projects.
```
sudo wget http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo
sudo yum install apache-maven -y
```

You can check that maven has been installed with `mvn --version`

**Note:** Ensure that Java 7 which is installed as a dependency to Apache Maven does not replace Java 8 as the default executable of the server.

Check Java versions with
```
java -version
javac -version
```

If they are not 1.8.x then you can amend with
```
sudo update-alternatives --config java
sudo update-alternatives --config javac
```

by selecting the jdk1.8.x option and then confirming the versions.

### Xvfb
Xvfb is a virtual X server on which UI-based tests will be rendered.

Install with
```
sudo yum install xorg-x11-server-Xvfb.x86_64 -y
```

Xvfb can be run manually, however it is better to have running as a service that can be started at boot.

#### Manually
```
Xvfb :99 -screen 5 1024x768x8 &
export DISPLAY=:99
```

#### Service
Create the file and add execute permissions
```
sudo touch /etc/init.d/xvfb
sudo chmod +x /etc/init.d/xvfb
```

Populate '/etc/init.d/xvfb' with
```
#!/bin/bash
#
# /etc/rc.d/init.d/xvfb
#
# chkconfig: 345 95 28
# description: Starts/Stops X Virtual Framebuffer server
# processname: Xvfb
#

. /etc/init.d/functions

[ "${NETWORKING}" = "no" ] && exit 0

PROG="/usr/bin/Xvfb"
PROG_OPTIONS=":99 -screen 5 1024x768x8"
PROG_OUTPUT="/tmp/Xvfb.out"

case "$1" in
    start)
        echo -n "Starting : X Virtual Frame Buffer "
        $PROG $PROG_OPTIONS>>$PROG_OUTPUT 2>&1 &
        disown -ar
        /bin/usleep 500000
        status Xvfb & >/dev/null && echo_success || echo_failure
        RETVAL=$?
        if [ $RETVAL -eq 0 ]; then
            /bin/touch /var/lock/subsys/Xvfb
            /sbin/pidof -o  %PPID -x Xvfb > /var/run/Xvfb.pid
        fi
        echo
        ;;
    stop)
        echo -n "Shutting down : X Virtual Frame Buffer"
        killproc $PROG
        RETVAL=$?
        [ $RETVAL -eq 0 ] && /bin/rm -f /var/lock/subsys/Xvfb /var/run/Xvfb.pid
        echo
        ;;
    restart|reload)
        $0 stop
        $0 start
        RETVAL=$?
        ;;
    status)
        status Xvfb
        RETVAL=$?
        ;;
    *)
     echo $"Usage: $0 (start|stop|restart|reload|status)"
     exit 1
esac

exit $RETVAL
```

You will also need to export the DISPLAY variable, so
```
sudo touch etc/profile.d/xvfb.sh
```

with the contents
```
export DISPLAY=:99
```
and the variable will be initialised at startup.

Add the service to load at startup
```
sudo chkconfig --add xvfb
```

You can now manually start, stop, restart and check the status of the service with
```
service xvfb start|stop|restart|status
```

To check that Xvfb is running `ps aux | grep -i xvfb` which should return something similar to
```
root      1240  0.6  2.5 110412 19144 ?        S    11:35   0:01 /usr/bin/Xvfb :99 -screen 5 1024x768x8
```


### Firefox
Selenium-based tests require one or more browsers (if compatibility testing is in scope) to be installed on Vitraux-Virtuoso.

Confirm the version of Firefox to install against Selenium compatibility. This will install Firefox 38.4 ESR
```
wget https://ftp.mozilla.org/pub/firefox/releases/38.4.0esr/linux-x86_64/en-GB/firefox-38.4.0esr.tar.bz2
sudo mv firefox-38.4.0esr.tar.bz2 /usr/local/
cd /usr/local
sudo tar xvfj firefox-38.4.0esr.tar.bz2
sudo ln -s /usr/local/firefox/firefox /usr/bin/firefox
```

If Xvfb is running, then you can check Firefox with `firefox -v`

### Filesystem configuration
Test Directory and Logging. Create the logging and test directory storage directories:
```
sudo mkdir -p /var/data/log/virtuoso && sudo chown -R vitraux.vitraux /var/data/log/virtuoso
sudo mkdir -p /var/data/virtuoso && sudo chown -R vitraux.vitraux /var/data/virtuoso
```

## Vitraux-Maestro
### Apache JMeter 2.13
Load testing tool
```
wget http://www.mirrorservice.org/sites/ftp.apache.org//jmeter/binaries/apache-jmeter-2.13.tgz
tar xvf apache-jmeter-2.13.tgz
sudo mkdir /usr/local/tools
sudo mv apache-jmeter-2.13 /usr/local/tools/apache-jmeter-2.13
```

### System Stats
The system statistics package offers many open source monitoring tools that can be used to monitor the system performance of the Vitraux-Maestro server during a performance test.
```
sudo yum install sysstat.x86_64 -y
```

### Gnuplot 4.6.4
Graph plotting tool to generate performance test result graphs. To generate better quality graphs, Gnuplot needs to be built from source, after the Cairo and Pango libraries have been installed.

```
sudo yum install gcc.x86_64 gcc-c++.x86_64 cairo-devel.x86_64 cairomm.x86_64 pango.x86_64 pango-devel.x86_64 cairo.x86_64 -y
wget http://sourceforge.net/projects/gnuplot/files/gnuplot/4.6.4/gnuplot-4.6.4.tar.gz/download
mv download gnuplot-4.6.4.tar.gz
tar xvfz gnuplot-4.6.4.tar.gz
cd gnuplot-4.6.4
./configure
```

Ensure that pango and cairo are configured as follows:
```
# checking for CAIROPANGO... yes
# checking for PANGO_1_10_2... no
# checking for CAIROPDF... yes
# checking for CAIROEPS... yes
```
```
make && sudo make install
```

The installation can be verified by running `gnuplot` and confirming that `pngcairo png terminal based on cairo` is shown in the list presented when typing `set term`.

### WebBrowserTest Private Instance
The WebBrowserTest instance can be used to perform page-level performance metrics for the Chrome browser within the Intranet that the Vitraux-Maestro server is installed. TBC

### Filesystem configuration
Test Directory and Logging. Create the logging and test directory storage directories:
```
sudo mkdir -p /var/data/log/maestro && sudo chown -R vitraux.vitraux /var/data/log/maestro
sudo mkdir -p /var/data/maestro && sudo chown -R vitraux.vitraux /var/data/maestro
```

## CentOS 6.5 installation complete
Now follow the [Build](#Build) steps

# CentOS 7
This has been built against CentOS-7-x86_64-Minimal-1503-01.iso

## Vitraux (Common)
### Deployment tools
If using the minimal installation of CentOS 7 then this does not include wget or some of the compression packages required
```
sudo yum install wget bzip2 unzip zip -y
```

### Java Development Kit 8 (JDK)
The Java virtual machine and development kit to compile Java files to bytecode.
```
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u66-b17/jdk-8u66-linux-x64.rpm"
```

Install
```
sudo yum install jdk-8u66-linux-x64.rpm -y
```

### Apache Tomcat 7
To install
```
sudo yum install tomcat.noarch -y
```

This will install Tomcat7 in `/usr/share/tomcat`

#### Firewall Config
By default, Vitraux will require TCP port 8080 to be open on the firewall, check with the system admin on how to configure and if different ports should be used.

If you have permission, this will open TCP port 8080
```
sudo firewall-cmd --zone=public --add-port=8080/tcp --permanent
sudo firewall-cmd --reload
```

To check rules `sudo iptables -L -n`

#### Permissions
Add vitraux to the tomcat group `sudo usermod -a -G tomcat vitraux`
This can be confirmed with `id vitraux`


Amend the user and group in `/usr/lib/systemd/system/tomcat.service` so that the vitraux user will execute the tomcat process
```
sudo sed -r -i 's/User=tomcat/User=vitraux/g' /usr/lib/systemd/system/tomcat.service
sudo sed -r -i 's/Group=tomcat/Group=vitraux/g' /usr/lib/systemd/system/tomcat.service
```

Add the user "vitraux" to `/usr/share/tomcat/conf/tomcat-users.xml`
```
sudo sed -r -i 's/<tomcat-users>/<tomcat-users>\n  <user username="vitraux" password="vitraux" roles="tomcat"\/>/g' /usr/share/tomcat/conf/tomcat-users.xml
```

Make vitraux the owner for the following folders
```
sudo chown -R vitraux.vitraux /var/cache/tomcat/
sudo chown -R vitraux.vitraux /usr/share/tomcat/webapps/
sudo chown -R vitraux.vitraux /var/log/tomcat/
sudo chown -R vitraux.vitraux /etc/tomcat/
sudo chown -R vitraux.vitraux /etc/sysconfig/tomcat
sudo chown -R vitraux.vitraux /etc/logrotate.d/tomcat
```

#### Service
How to start/stop/restart the service and add to startup if required.

- Start `sudo systemctl start tomcat`
- Stop `sudo systemctl stop tomcat stop`
- Restart `sudo systemctl restart tomcat`
- Enable on startup `sudo systemctl enable tomcat`

Start the service.

Check that the vitraux user is running the tomcat service and not the tomcat user with `ps aux | grep tomcat`. The user is the first entry -
```
vitraux   3063 25.2  7.1 2201500 54592 ?       Ssl  16:02   0:01 /usr/bin/java -classpath /usr/share/tomcat/bin/bootstrap.jar:/usr/share/tomcat/bin/tomcat-juli.jar:/usr/share/java/commons-daemon.jar -Dcatalina.base=/usr/share/tomcat -Dcatalina.home=/usr/share/tomcat -Djava.endorsed.dirs= -Djava.io.tmpdir=/var/cache/tomcat/temp -Djava.util.logging.config.file=/usr/share/tomcat/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager org.apache.catalina.startup.Bootstrap start
```

Enable the service on startup

### MySQL Server
Database server used to hold information on test assemblies and runs performed on this Vitraux server

The following will install MySQL Server and set the service to run on startup. The database will be configured with

- User = `root`
- Password = `admin`

```
sudo yum install mariadb-server mariadb -y
sudo systemctl start mariadb.service
sudo systemctl enable mariadb.service
sudo mysqladmin -u root password admin
```

If you want to check the database `mysql -u root -p` and enter the password. To show the databases available `show databases;` which should show 4 by default. To exit use `exit`

### Results Directory

Create the results directory in Tomcat webapps:
```
sudo mkdir /usr/share/tomcat/webapps/results && sudo chown -R vitraux.vitraux /usr/share/tomcat/webapps/results
```

## Vitraux-Virtuoso
### Apache Maven 3.0
Build manager for Java projects.
```
sudo wget http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo
sudo yum install apache-maven -y
```

You can check that maven has been installed with `mvn --version`

**Note:** Ensure that Java 7 which is installed as a dependency to Apache Maven does not replace Java 8 as the default executable of the server.

Check Java versions with
```
java -version
javac -version
```

If they are not 1.8.X then you can amend with
```
sudo update-alternatives --config java
sudo update-alternatives --config javac
```

by selecting the jdk1.8.X option and then confirming the versions.

### Xvfb
The Xvfb is a virtual X server on which UI-based tests will be rendered.

Install with
```
sudo yum install xorg-x11-server-Xvfb.x86_64 -y
```

Xvfb can be run manually, however it is better to have running as a service that can be started at boot.

#### Manually
```
Xvfb :99 -screen 5 1024x768x8 &
export DISPLAY=:99
```

#### Service
Create the file and add execute permissions
```
sudo touch /etc/systemd/system/xvfb.service
sudo chmod +x /etc/systemd/system/xvfb.service
```

Add the following to `/etc/systemd/system/xvfb.service`
```
[Unit]
Description=X Virtual Frame Buffer Service
After=network.target

[Service]
ExecStart=/usr/bin/Xvfb :99 -screen 5 1024x768x8

[Install]
WantedBy=multi-user.target
```

To enable the service at startup, run
```
sudo systemctl enable xvfb.service
```

You will also need to export the DISPLAY variable, so create
```
sudo touch /etc/profile.d/xvfb.sh
```

with the contents
```
export DISPLAY=:99
```
and the variable will be initialised at startup.

You can now manually start, stop, restart and check the status of the service with `systemctl start|stop|restart|status xvfb`

To check that Xvfb is running `ps aux | grep -i xvfb` which should return something similar to
```
root      1240  0.6  2.5 110412 19144 ?        S    11:35   0:01 /usr/bin/Xvfb :99 -screen 5 1024x768x8
```

### Firefox
Selenium-based tests require one or more browsers (if compatibility testing is in scope) to be installed on Vitraux-Virtuoso.

Confirm the version of Firefox to install against Selenium compatibility. This will install Firefox 38.4 ESR
```bash
wget https://ftp.mozilla.org/pub/firefox/releases/38.4.0esr/linux-x86_64/en-GB/firefox-38.4.0esr.tar.bz2
sudo mv firefox-38.4.0esr.tar.bz2 /usr/local/
cd /usr/local
sudo tar xvfj firefox-38.4.0esr.tar.bz2
sudo ln -s /usr/local/firefox/firefox /usr/bin/firefox
```

If Xvfb is running, then you can check Firefox with `firefox -v`

### Google Chrome
This installs the latest stable version of Google Chrome for use in Vitraux-Virtuoso
```
echo -e "[google-chrome]\nname=google-chrome\nbaseurl=http://dl.google.com/linux/chrome/rpm/stable/"'$'"basearch\nenabled=1\ngpgcheck=1\ngpgkey=https://dl-ssl.google.com/linux/linux_signing_key.pub" > google-chrome.repo
sudo mv google-chrome.repo /etc/yum.repos.d/
sudo yum install google-chrome-stable -y
```

### Selenium ChromeDriver
ChromeDriver is a standalone server which implements WebDriver's wire protocol for Chrome.
```
wget http://chromedriver.storage.googleapis.com/2.20/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
sudo mv chromedriver /usr/bin/
```

### Filesystem configuration
Test Directory and Logging. Create the logging and test directory storage directories:
```
sudo mkdir -p /var/data/log/virtuoso && sudo chown -R vitraux.vitraux /var/data/log/virtuoso
sudo mkdir -p /var/data/virtuoso && sudo chown -R vitraux.vitraux /var/data/virtuoso
```

## Vitraux-Maestro
### Apache JMeter 2.13
Load testing tool
```
wget http://www.mirrorservice.org/sites/ftp.apache.org//jmeter/binaries/apache-jmeter-2.13.tgz
tar xvf apache-jmeter-2.13.tgz
sudo mkdir /usr/local/tools
sudo mv apache-jmeter-2.13 /usr/local/tools/apache-jmeter-2.13
```

### System Stats
The system statistics package offers many open source monitoring tools that can be used to monitor the system performance of the Vitraux-Maestro server during a performance test.
```
sudo yum install sysstat.x86_64 -y
```

### Gnuplot 4.6.4
Graph plotting tool to generate performance test result graphs. To generate better quality graphs, Gnuplot needs to be built from source, after the Cairo and Pango libraries have been installed.
```
sudo yum install gcc.x86_64 gcc-c++.x86_64 cairo-devel.x86_64 cairomm.x86_64 pango.x86_64 pango-devel.x86_64 cairo.x86_64 -y
wget http://sourceforge.net/projects/gnuplot/files/gnuplot/4.6.4/gnuplot-4.6.4.tar.gz/download
mv download gnuplot-4.6.4.tar.gz
tar xvfz gnuplot-4.6.4.tar.gz
cd gnuplot-4.6.4
./configure
```

Ensure that pango and cairo are configured as follows:
```
# checking for CAIROPANGO... yes
# checking for PANGO_1_10_2... no
# checking for CAIROPDF... yes
# checking for CAIROEPS... yes
```
```
make && sudo make install
```

The installation can be verified by running `gnuplot` and confirming that `pngcairo png terminal based on cairo` is shown in the list presented when typing `set term`.

### WebBrowserTest Private Instance
The WebBrowserTest instance can be used to perform page-level performance metrics for the Chrome browser within the Intranet that the Vitraux-Maestro server is installed. TBC

### Filesystem configuration
Test Directory and Logging. Create the logging and test directory storage directories:
```
sudo mkdir -p /var/data/log/maestro && sudo chown -R vitraux.vitraux /var/data/log/maestro
sudo mkdir -p /var/data/maestro && sudo chown -R vitraux.vitraux /var/data/maestro
```

## CentOS 7 installation complete
Now follow the [Build](#Build) steps



# Build
These steps are applicable to CentOS 6.5 and CentOS 7

Vitraux 1.4 onwards is compatible with Maven 3.0. It is built by running:

To generate the Vitraux-Virtuoso servlet war
```
mvn clean install -Pvirtuoso
```

To generate the Vitraux-Maestro servlet war
```
mvn clean install -Pmaestro
```

The install requires that `virtuoso.jar` and `maestro.jar` and associated dependencies exist in the local repository `/user/vitraux/.m2/repository/`. If the do not exist then download and build with
```
mvn clean install
```

# Deploying
## Database
The `./src/main/resources/db/database.sql` can be used to set up the MySQL “vitraux” database. This database is used as a resource in the `context.xml` files of the deployed servlets.

From within the Vitraux source directory
```
cd src/main/resources/db
mysql -u root -p < database.sql
```

To check that the tables have deployed, connect to the vitraux database `mysql -u root -p -D vitraux`. View the tables with `show tables;` which should look like
```
+----------------------------------+
| Tables_in_vitraux                |
+----------------------------------+
| ci_environment                   |
| perftest_suite                   |
| perftest_suite_execution         |
| perftest_suite_execution_results |
+----------------------------------+
```

To exit mysql, type `exit`


## Servlet
The generated war files can be deployed by being copied to the `webapps/` directory of an Apache Tomcat installation.

For all versions of Vitraux, configuration files (see [Configuration](vitraux/configuration.md#webxml)), like the `web.xml`, `config.properties`, and `log4j.properties` files are preconfigured for these war files with the default values found in the Configuration table. One might change these values either before building the war files, or after deploying them.

Specifically, these configuration files can be found under:
`./WEB-INF/web.xml`
`./WEB-INF/classes/config.properties`
`./WEB-INF/classes/log4j.properties`

Manually modifying these values and restarting the Tomcat server should suffice for the new values to take effect.

### CentOS 7
Specifically for CentOS 7 amend
`../webapps/virtuoso/META-INF/context.xml` and
`../webapps/maestro/META-INF/context.xml` to include `factory="org.apache.commons.dbcp.BasicDataSourceFactory"`

The file should look similar to (make sure to move `/>` from maxIdle)
```
<?xml version='1.0' encoding='utf-8'?>
<Context>
<Resource name="jdbc/vitraux"
        auth="Container"
        type="javax.sql.DataSource"
        username="root"
        password="admin"
        driverClassName="com.mysql.jdbc.Driver"
        url="jdbc:mysql://localhost:3306/vitraux"
        maxActive="15"
        maxIdle="3"
        factory="org.apache.commons.dbcp.BasicDataSourceFactory" />
</Context>
```

### Default Installation
If you have followed the default installation steps then you should only need to check

`../webapps/virtuoso/WEB-INF/classes/config.properties`
Check that `Default.OutputFile` and `Default.ReportFile` matches your Tomcat installation directory

`../webapps/maestro/WEB-INF/classes/config.properties`
Check that `Default.OutputFile` and `Default.ReportFile` matches your Tomcat installation directory
Check that `JMeter.Executable` and `JMeter.Jar` match the path and version installed

If values are amended then restart the tomcat server

## Validation
Once all setup activities have taken place, then the installation can be validated as:
```bash
curl <vitraux url>:<vitraux port>/virtuoso/VirtuosoServer?type=status
curl <vitraux url>:<vitraux port>/maestro/MaestroServer?type=status
```

If running locally on the Vitraux server itself with the default port this would be :
```bash
curl localhost:8080/virtuoso/VirtuosoServer?type=status
curl localhost:8080/maestro/MaestroServer?type=status
```

Both validation request should return:
```bash
Server active: 0 test suites completed 0 to be processed
```

# Troubleshooting

## Tomcat

### Error - null
If you the see following error when you curl the service -
```
Error -  null
	uk.gov.homeoffice.maestro.server.Server doGet(87)
	javax.servlet.http.HttpServlet service(620)
	javax.servlet.http.HttpServlet service(727)
	org.apache.catalina.core.ApplicationFilterChain internalDoFilter(303)
	org.apache.catalina.core.ApplicationFilterChain doFilter(208)
	org.apache.tomcat.websocket.server.WsFilter doFilter(52)
	org.apache.catalina.core.ApplicationFilterChain internalDoFilter(241)
	org.apache.catalina.core.ApplicationFilterChain doFilter(208)
	org.apache.catalina.core.StandardWrapperValve invoke(220)
	org.apache.catalina.core.StandardContextValve invoke(122)
	org.apache.catalina.authenticator.AuthenticatorBase invoke(501)
	org.apache.catalina.core.StandardHostValve invoke(171)
	org.apache.catalina.valves.ErrorReportValve invoke(102)
	org.apache.catalina.valves.AccessLogValve invoke(950)
	org.apache.catalina.core.StandardEngineValve invoke(116)
	org.apache.catalina.connector.CoyoteAdapter service(408)
	org.apache.coyote.http11.AbstractHttp11Processor process(1040)
	org.apache.coyote.AbstractProtocol$AbstractConnectionHandler process(607)
	org.apache.tomcat.util.net.JIoEndpoint$SocketProcessor run(314)
	java.util.concurrent.ThreadPoolExecutor runWorker(1142)
	java.util.concurrent.ThreadPoolExecutor$Worker run(617)
	org.apache.tomcat.util.threads.TaskThread$WrappingRunnable run(61)
	java.lang.Thread run(745)
```

Make sure that `factory="org.apache.commons.dbcp.BasicDataSourceFactory" />` exists for `/webapps/maestro/META-INF/context.xml` and `../webapps/maestro/META-INF/context.xml`

## Xvfb
### Error: no display specified
If you see an error such as
```
org.openqa.selenium.firefox.NotConnectedException: Unable to connect to host 127.0.0.1 on port 7055 after 45000 ms. Firefox console output:

(process:18996): GLib-CRITICAL **: g_slice_set_config: assertion 'sys_page_size == 0' failed
Error: no display specified
```

Check that the the DISPLAY variable has been set `echo $DISPLAY` and that the output matches how Xvfb has been started `ps aux | grep -i xvfb` and configured in Vitraux-Virtuoso `../webapps/virtuoso/WEB-INF/web.xml`
```
  <context-param>
        <param-name>seleniumDisplay</param-name>
        <param-value>:99</param-value>
  </context-param>
```

### etc/hosts
Within `/etc/hosts` the entry for 127.0.0.1 must start with localhost, followed by any other domain names, such as
```
127.0.0.1   localhost localhost.localdomain
```

## Selenium/Firefox
Selenium support for Firefox is for the latest release, the previous release, the latest ESR release and the previous ESR release.

Check what version of Selenium is being used by looking at the Virtuoso pom.xml for the tag `<selenium-server.version>2.50.1</selenium-server.version>`

As of November 2015 Selenium 2.50.1 will support [Firefox ESR](https://www.mozilla.org/en-US/firefox/organizations/faq/)
- ESR 38.x (current)
- ESR 31.8.0 (previous)
- The next ESR 45.X is expected in March 2016 when ESR 31.8 will no longer be officially supported

## Standalone Xvfb and Firefox Test
This test requires the [Selenium Standalone Server](http://goo.gl/76CpPA) which will need to be downloaded.

Create `SeleniumFirefoxTest.java`. You may need to amend the URL if you are not able to access www.google.com to something that can be accessed by the Vitraux server. For instance if you also have the Virtuoso Listener running you can amend to `String URL="localhost:8080/virtuoso-listener-2.0.1/admin/help";`

```java
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumFirefoxTest {
  public static void main(String[] args) throws Exception {

    String URL="http://www.google.com/";

    System.out.println("Starting Selenium Firefox Test");

    // The Firefox driver supports javascript
    WebDriver driver = new FirefoxDriver();
    System.out.println("Firefox driver initialised");

    // Go to the Google Suggest home page
    driver.get(URL);
    System.out.println("Navigating to " + URL);

    // Check the title of the page
    System.out.println("Page title is: " + driver.getTitle());

    // Check HTMl Contents
    WebElement element = driver.findElement(By.xpath("//*"));
    System.out.println(element);

	driver.quit();
    System.out.println("Quit Firefox driver");

    System.out.println("-------------");
    System.out.println("Test Complete");

  }
}
```

Make sure the Selenium Standalone Server ie. `selenium-server-standalone-2.50.1.jar` is in the same directory and then compile - `javac -cp selenium-server-standalone-2.50.1.jar SeleniumFirefoxTest.java`

Run `java -cp .:./selenium-server-standalone-2.50.1.jar SeleniumFirefoxTest` which will call Firefox via the Xvfb navigate to the specified URL and display the page title and some local information. If this completes without error then Xvfb is working correctly.

## Vitraux

### JAVA_HOME
```
Virtuoso    [INFO]:   Error: JAVA_HOME is not defined correctly.
Virtuoso    [INFO]:     We cannot execute /usr/lib/jvm/jre/bin/java
```

Check that the virtuoso library and associated dependencies have been deployed.

### MySQL
Within Jenkins if you receive the error
```
apt-plugin  [ERROR]:   Error: Error reported from the Vitraux server side Failure - Cannot load JDBC driver class 'com.mysql.jdbc.Driver'
```

Check that `mysql-connector-java-5.1.37-bin.jar` is correctly installed.

# Installation Tips

## VirtualBox
If you are running locally on a virtual machine (such as VirtualBox) it can be helpful to run with two network adapters to keep a locally static IP.
- Adapter 1: NAT
- Adapter 2: Host-only Adapter: vboxnet0

NAT will give you internet access, while the host-only will give you a local static IP address. You will need to make sure you have both adapters e.g. `ifcfg-eth0` and `ifcfg-eth1` configured as described in Networking

If you want to choose the IP address, rather than have it assigned by VirtualBox amend the `BOOTPROTO=dhcp` reference and add ann ip address and netmask.
```
BOOTPROTO=none
IPADDR=192.168.56.XXX
NETMASK=255.255.255.0
```

## Networking
To autostart the network adapters, navigate to `/etc/sysconfig/network-scripts` and edit the file for the network adapter, the default is `ifcfg-eth0` and amend to `ONBOOT=yes`

Restart the networking service `sudo service network restart`

Run `ip addr` to check your IP address.

## Vitraux Users
To create the Vitraux user and grant sudo permissions
```
sudo useradd vitraux
sudo passwd vitraux
sudo gpasswd -a vitraux wheel
```

Edit sudoers `visudo` and add the vitraux user below root
```
## Allow root to run any commands anywhere
root	ALL=(ALL)	ALL
vitraux	ALL=(ALL)	ALL
```