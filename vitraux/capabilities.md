The current version of the Vitraux Server is 1.4. The supported capabilities are:

## Vitraux 1.4 
- Maven 3.0 support for automated test execution
- Multiple environment property files support
- Support of separation of test data and test suite for automated tests

## Vitraux 1.31
- Execution of bundled jar files compiled with Virtuoso
- JMeter and Webpagetest support for performance tests

