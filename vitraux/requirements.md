For a test to be executed successfully on Vitraux, the following conditions need to apply:

## Vitraux ##
- Vitraux needs to be accessible from Jenkins and have access to the services of the SUT.
- The “vitraux” user should be executing the Tomcat server.
- All paths of the config.properties and log4j.properties files need to be valid, i.e. existing and readable or writeable by the “vitraux” user that executes the Tomcat server.
- The “vitraux” MySQL database needs to exist and be accessible as defined in the context.xml of the Vitraux-Virtuoso and Vitraux-Maestro servlets.

## Vitraux-Virtuoso ##
- Vitraux-Virtuoso requires that the root of the automation test suite zip (see Jenkins plugin usage) contains a pom.xml.
- Test data that are sent as a separate zip (version 1.4 onwards) need to contain all necessary test data at the root of the zip file. They will be extracted to src/tests/resources/test-data/.
- Vitraux-Virtuoso needs to be able to resolve dependencies via Maven using an artefact repository that contains the versions of the Virtuoso library or any other third-party library that are used by the test automation suites.
- Vitraux-Virtuoso requires a valid X server display for headless execution, i.e Xvfb needs to be running.
- The browsers that are used in Selenium-based tests should exist and any driver executables, e.g. chromedriver, need to be in the default PATH of  the “vitraux” user.

## Vitraux-Maestro ##
- Vitraux-Maestro requires that the root of the performance test suite zip (see Jenkins plugin usage) contains a plan.xml. All paths within the plan.xml need to be relative so that they can be resolved correctly in the extracted test suite zip file.
- Vitraux-Maestro needs to use Gnuplot with pngcairo enabled.
- Vitraux-Maestro requires the /tmp/ directory to exist and be writeable by the “vitraux” user.
- Vitraux-Maestro that the “vitraux” user is able to ssh to any remote server that is monitored according to the plan.xml.
- Typical Unix tools like grep, sed, awk, and curl need to exist on the Vitraux-Maestro server.


