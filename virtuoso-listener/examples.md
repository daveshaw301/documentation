# Examples
## Case 1: SOAP Web Service Stubbing
A clean VL instance is assumed on `localhost:8080`.

In this example the VL will be used to replace a SOAP web service. It will accept SOAP 1.2 messages with the namespace `vl-soap-example` as `POST` requests, extract the message id from these request (&lt;id> ... &lt;/id>), like:

```xml
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <Enlighten xmlns="vl-soap-example">
	  <licenseID>string</licenseID>
	  <content>string</content>
	  <id>123</id>
	  <paramsXML>string</paramsXML>
    </Enlighten>
  </soap:Body>
</soap:Envelope>
```

and return it in a response body of the format:

```xml
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <Timestamp>time of incoming request in seconds</Timestamp>
    <EnlightMsgId>id extracted from the incoming message</EnlightMsgId>
    <EnlightenResult>PASS</EnlightenResult>
    </EnlightenResponse>
  </soap:Body>
</soap:Envelope>
```

### Step 1: Configure the VL message type to accept this SOAP request
The 'Soap12ActionMatcher' matching rule will be used to register the message type for this type requests. Using the VL library, the following object is defined:

```java
Matcher<HttpServletRequest> messageMatchingCriteria = 
new AndMatcher<HttpServletRequest>(Method.POST,
new Soap12ActionMatcher(new ExactStringMatcher("{vl-soap-example}Enlighten")));
```

The extraction rules involve getting the value of the `<id>` element from the request body. Using the VL library, the following object is defined:

```java
Extractor extractorRules = 
new CompositeExtractor<>(new RegexpExtractor("<id>([0-9]*)</id>", "$1$"), new BodyExtractor());
```

These objects are serialized and printed as:

```java
MessageType messageType = new MessageType(messageMatchingCriteria, extractorRules);
System.out.println(new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true).writeValueAsString(messageType));
```

The output for the message type registration JSON, `tutorial1-type.json`, is:

```json
{
  "category" : {
    "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
    "matchers" : [ {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "POST"
      }
    }, {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.Soap12ActionMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "{vl-soap-example}Enlighten"
      }
    } ]
  },
  "id-extractor" : {
    "extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.composite.CompositeExtractor",
    "left-func" : {
      "extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.string.RegexpExtractor",
      "regexp" : "<id>([0-9]*)</id>",
      "template" : "$1$"
    },
    "right-func" : {
      "extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.request.BodyExtractor"
    }
  }
}
```

Using curl, the VL can be configured for the message type as follows:

```bash
curl --data @tutorial1-type.json --header 'Content-Type: application/json' http://localhost:8080/virtuoso-listener-2.0/admin/register-type
```

### Step 2: Configure the VL response type to send appropriate responses
The same matching criteria as in Step 1 will be used to define response types. Using the VL library, the following object is defined:

```java
Matcher<HttpServletRequest> responseMatchingCriteria = 
new AndMatcher<HttpServletRequest>(Method.POST,
new Soap12ActionMatcher(new ExactStringMatcher("{vl-soap-example}Enlighten")));
```

The response template for this message should extract the `<id>` element from the request body and replace it dynamically on the response body. Using the VL library, the following object is defined:

```java
ResponseTemplate responseTemplate = new ResponseTemplate("" +
 "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
 "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" 
 xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
 "<soap:Body>\n" +
 "  <Timestamp>${TIMESTAMP:EEE, d MMM yyyy HH:mm:ss.S}</Timestamp>\n" +
 "  <EnlightMsgId>${REGEXP:<id>([0-9]*)</id>}</EnlightMsgId>\n" +
 "  <EnlightenResult>PASS</EnlightenResult>\n" +
 "  </EnlightenResponse>\n" +
 "  </soap:Body>\n" +
 "</soap:Envelope>", 200, 0L, ResponseTemplate.Type.DYNAMIC);
```
 
These objects are serialized and printed as:

```java
ResponseType responseType = new ResponseType(requestMatchingCriteria, responseTemplate);
System.out.println(new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true).writeValueAsString(responseType));
```

The output for the response type registration JSON, `tutorial1-response.json`, is:

```json
{
  "category" : {
    "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
    "matchers" : [ {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "POST"
      }
    }, {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.Soap12ActionMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "{vl-soap-example}Enlighten"
      }
    } ]
  },
  "response-template" : {
    "body" : "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soap:Body>\n  <Timestamp>${TIMESTAMP:EEE, d MMM yyyy HH:mm:ss.S}</Timestamp>\n  <EnlightMsgId>${REGEXP:<id>([0-9]*)</id>}</EnlightMsgId>\n  <EnlightenResult>PASS</EnlightenResult>\n  </EnlightenResponse>\n  </soap:Body>\n</soap:Envelope>",
    "status" : 200,
    "thinktime" : 0,
    "type" : "DYNAMIC"
  }
}
```

Using curl, the VL can be configured for the response type as follows:

```bash
curl --data @tutorial1-response.json --header 'Content-Type: application/json' http://localhost:8080/virtuoso-listener-2.0/admin/register-response
```

The registered message and response types can be confirmed by running:

```bash
curl http://localhost:8080/virtuoso-listener-2.0/admin/configuration
```

### Step 3: Send messages to the VL
Defining `tutorial1.xml` as:

```xml
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <Enlighten xmlns="vl-soap-example">
	<licenseID>string</licenseID>
	<content>string</content>
	<id>123</id>
	<paramsXML>string</paramsXML>
    </Enlighten>
  </soap:Body>
</soap:Envelope>
```

then, being configured in Steps 1 and 2, the VL will respond to:

```bash
curl -X POST --data @tutorial1.xml --header 'Content-Type: application/soap+xml' http://localhost:8080/virtuoso-listener-2.0/service/test
```

with:

```xml
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Body>
  <Timestamp>Wed, 14 Oct 2015 10:08:52.961</Timestamp>
  <EnlightMsgId>123</EnlightMsgId>
  <EnlightenResult>PASS</EnlightenResult>
  </EnlightenResponse>
  </soap:Body>
</soap:Envelope>
```

This means that:
- The incoming SOAP request was matched against the registered criteria, as it was a POST request and the Soap12ActionMatcher found the right action.
- The unique 'id' for that message was correctly extracted.
- The dynamic response was correctly created based on the template.

### Step 4: Search for stored messages 
Defining the search criteria, `tutorial1-search.json`, as:

```json
{
    "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
    "matchers" : [ {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "POST"
      }
    }, {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.Soap12ActionMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "{vl-soap-example}Enlighten"
      }
    } ]
  }
```

then, having received at least one message in Step 3, the VL will respond to:

```bash
curl -X POST --data @tutorial1-search.json --header 'Content-Type: application/json' http://localhost:8080/virtuoso-listener-2.0/admin/search
```

with:

```json
[
  {
    "loggedDate":1444756132960,
    "method":"POST",
    "pathInfo":"/test",
    "url":"http://localhost:8080/virtuoso-listener-2.0/service/test",
    "parameters":{},
    "headers":{
      "user-agent":"curl/7.35.0",
      "host":"localhost:8080",
      "accept":"*/*",
      "content-type":"application/soap+xml",
      "content-length":"404"
    },
    "remoteAddress":"127.0.0.1",
    "body":"<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">  <soap:Body>    <Enlighten xmlns=\"vl-soap-example\">\t<licenseID>string</licenseID>\t<content>string</content>\t<id>123</id>\t<paramsXML>string</paramsXML>    </Enlighten>  </soap:Body></soap:Envelope>"
  }
]
```

## Case 2: REST API stubbing
A clean VL instance is assumed on `localhost:8080`.

In this example the VL will be used to replace a REST-ful web service. It will accept two types of messages: 

#### Type 1
POST messages to a `/register` path with parameters 'username' and 'password-md5' to which it will respond with a '200' response with body:

```html
<html>
	<body>
		Registration successful!
	</body>
</html>
```
#### Type 2
GET messages that have an 'Authentication-Key' header with 64 base64 characters, to which it will respond with '200' and an empty response.

#### Type 3 - Others
Any other GET or POST message should be responded with '403'.

### Step 1: Configure the VL message types
#### Register message Type 1
To register the message type for requests of type 1, the 'MethodMatcher' and the 'ParameterMatcher' matching criteria will be combined. Using the VL library, the following object is defined:

```java
Matcher<HttpServletRequest> messageMatchingCriteria = new AndMatcher<HttpServletRequest>(
        Method.POST,
        new ParameterMatcher("username", new RegexpStringMatcher("^[A-Za-z0-9]+$")),
        new ParameterMatcher("password-md5", new RegexpStringMatcher("^[A-Za-z0-9]+$")),
        new PathInfoMatcher(new ContainsStringMatcher("/register")));
```

Although the extraction rules have not been formally defined, a reasonable choice for unique message key is the “username” parameter. Using the VL library, the following object is defined:

```java
Extractor extractorRules = new CompositeExtractor<>(new SingletonExtractor(), new ParameterExtractor("username"));
```
These objects are serialized and printed as:

```java
MessageType messageType = new MessageType(messageMatchingCriteria, extractorRules);
System.out.println(new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true).writeValueAsString(messageType));
```

The output for the message type registration JSON, `tutorial2-type1.json`, is:

```json
{
  "category" : {
    "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
    "matchers" : [ {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "POST"
      }
    }, {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.ParameterMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.RegexpStringMatcher",
        "matcher-target" : "^[A-Za-z0-9]+$"
      },
      "parameter-key" : "username"
    }, {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.ParameterMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.RegexpStringMatcher",
        "matcher-target" : "^[A-Za-z0-9]+$"
      },
      "parameter-key" : "password-md5"
    }, {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.PathInfoMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ContainsStringMatcher",
        "matcher-target" : "/register"
      }
    } ]
  },
  "id-extractor" : {
    "extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.composite.CompositeExtractor",
    "left-func" : {
      "extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.string.SingletonExtractor"
    },
    "right-func" : {
      "extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.request.ParameterExtractor",
      "parameter-key" : "username"
    }
  }
}
```

Using curl, the VL can be configured for message type 1 as follows:

```bash
curl --data @tutorial2-type1.json --header 'Content-Type: application/json' http://localhost:8080/virtuoso-listener-2.0/admin/register-type
```

#### Part 2: Register message Type 2
To register the message type for requests of type 2, the 'MethodMatcher' and the 'HeaderMatcher' matching criteria will be combined. Using the VL library, the following object is defined:will be combined. Using the VL library, the following object is defined:

```java
Matcher<HttpServletRequest> messageMatchingCriteria = new AndMatcher<HttpServletRequest>(Method.GET,
new HeaderMatcher("Authentication-Key", new RegexpStringMatcher("^[A-Za-z0-9+=/]+$")));
```

Although the extraction rules have not been formally defined, a reasonable choice for unique message key is the Authentication-Key header itself. Using the VL library, the following object is defined:

```java
Extractor extractorRules = new CompositeExtractor<>(new SingletonExtractor(), new HeaderExtractor("Authentication-Key"));
```

These objects are serialized and printed as:

```java
MessageType messageType = new MessageType(messageMatchingCriteria, extractorRules);
System.out.println(new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true).writeValueAsString(messageType));
```

The output for the message type registration JSON, `tutorial2-type2.json`, is:

```json
{
  "category" : {
    "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
    "matchers" : [ {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "GET"
      }
    }, {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.HeaderMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.RegexpStringMatcher",
        "matcher-target" : "^[A-Za-z0-9+=/]+$"
      },
      "header-field-name" : "Authentication-Key"
    } ]
  },
  "id-extractor" : {
    "extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.composite.CompositeExtractor",
    "left-func" : {
      "extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.string.SingletonExtractor"
    },
    "right-func" : {
      "extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.request.HeaderExtractor",
      "header-field-name" : "Authentication-Key"
    }
  }
}
```

Using curl, the VL can be configured for the message type as follows:

```bash
curl --data @tutorial2-type2.json --header 'Content-Type: application/json' http://localhost:8080/virtuoso-listener-2.0/admin/register-type
```

#### Part 3: Register message Type 3
Requests of type 3 should match against any other message, so that they can be configured to send a response of '403'.

```java
Matcher<HttpServletRequest> messageMatchingCriteria = new OrMatcher<HttpServletRequest>(Method.GET, Method.POST);
```

The message store will maintain the path info for the discarded incoming requests:

```java
Extractor extractorRules = new CompositeExtractor<>(new SingletonExtractor(), new PathInfoExtractor());
```

These objects are serialized and printed as:

```java
MessageType messageType = new MessageType(messageMatchingCriteria, extractorRules);
System.out.println(new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true).writeValueAsString(messageType));
```

The output for the message type registration JSON, `tutorial2-type3.json`, is:

```json
{
  "category" : {
    "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.OrMatcher",
    "matchers" : [ {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "GET"
      }
    }, {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "POST"
      }
    } ]
  },
  "id-extractor" : {
    "extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.composite.CompositeExtractor",
    "left-func" : {
      "extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.string.SingletonExtractor"
    },
    "right-func" : {
      "extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.request.PathInfoExtractor"
    }
  }
}
```

Using curl, the VL can be configured for the message type as follows:

```bash
curl --data @tutorial2-type3.json --header 'Content-Type: application/json' http://localhost:8080/virtuoso-listener-2.0/admin/register-type
```

### Step 2: Configure the VL response type to send appropriate responses
The same matching criteria as in Step 1 will be used to define response types. Using the VL library, the following object is defined:

```java
Matcher<HttpServletRequest> messageMatchingCriteria = new AndMatcher<HttpServletRequest>(
        Method.POST,
        new ParameterMatcher("username", new RegexpStringMatcher("^[A-Za-z0-9]+$")),
        new ParameterMatcher("password-md5", new RegexpStringMatcher("^[A-Za-z0-9]+$")),
        new PathInfoMatcher(new ContainsStringMatcher("/register")));;
```

The response template for this message should extract the `<id>` element from the request body and replace it dynamically on the response body. Using the VL library, the following object is defined:

```java
ResponseTemplate responseTemplate = new ResponseTemplate("<html>\n" +
        "<body>\n" +
        "Registration successful!\n" +
        "</body>\n" +
        "</html>", 200, 0L, ResponseTemplate.Type.STATIC);      
```

These objects are serialized and printed as:

```java
ResponseType responseType = new ResponseType(requestMatchingCriteria, responseTemplate);
System.out.println(new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true).writeValueAsString(responseType));
```

The output for the response type registration JSON, `tutorial2-response1.json`, is:

```json
{
  "category" : {
    "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
    "matchers" : [ {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "POST"
      }
    }, {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.ParameterMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.RegexpStringMatcher",
        "matcher-target" : "^[A-Za-z0-9]+$"
      },
      "parameter-key" : "username"
    }, {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.ParameterMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.RegexpStringMatcher",
        "matcher-target" : "^[A-Za-z0-9]+$"
      },
      "parameter-key" : "password-md5"
    }, {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.PathInfoMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ContainsStringMatcher",
        "matcher-target" : "/register"
      }
    } ]
  },
  "response-template" : {
    "body" : "<html>\n<body>\nRegistration successful!\n</body>\n</html>",
    "status" : 200,
    "thinktime" : 0,
    "type" : "STATIC"
  }
}
```

Using curl, the VL can be configured for the response type as follows:

```bash
curl --data @tutorial2-response1.json --header 'Content-Type: application/json' http://localhost:8080/virtuoso-listener-2.0/admin/register-response
```

Although response type 2 is the same as the default one, it has to be explicitly registered, because otherwise incoming messages would be matched against the more generic response type 3 and receive a 403 response code.

The matching criteria are the same as in the message type 2 definition:

```java
Matcher<HttpServletRequest> messageMatchingCriteria = new AndMatcher<HttpServletRequest>(Method.GET,
new HeaderMatcher("Authentication-Key", new RegexpStringMatcher("^[A-Za-z0-9+=/]+$")));
```

The response template is an empty body with a 200 response code. This is defined as follows:

```java
ResponseTemplate responseTemplate = new ResponseTemplate("", 200, 0L, ResponseTemplate.Type.STATIC);
```

These objects are serialized and printed as:

```java
ResponseType responseType = new ResponseType(requestMatchingCriteria, responseTemplate);
System.out.println(new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true).writeValueAsString(responseType));
```

The output for the message type registration JSON, `tutorial2-response2.json`, is:

```json
{
  "category" : {
    "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
    "matchers" : [ {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "GET"
      }
    }, {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.HeaderMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.RegexpStringMatcher",
        "matcher-target" : "^[A-Za-z0-9+=/]+$"
      },
      "header-field-name" : "Authentication-Key"
    } ]
  },
  "response-template" : {
    "body" : "",
    "status" : 200,
    "thinktime" : 0,
    "type" : "STATIC"
  }
}
```

Using curl, the VL can be configured for the message type as follows:

```bash
curl --data @tutorial2-response2.json --header 'Content-Type: application/json' http://localhost:8080/virtuoso-listener-2.0/admin/register-response
```

All other GET and POST messages need to be responded with 403. The matching criteria are the same as in the message type 3 definition:

```java
Matcher<HttpServletRequest> messageMatchingCriteria = 
new OrMatcher<HttpServletRequest>(Method.GET, Method.POST);
```

The response template is an empty body with a 403 response code. This is defined as follows:

```java
ResponseTemplate responseTemplate = 
new ResponseTemplate("", 403, 0L, ResponseTemplate.Type.STATIC);
```

These objects are serialized and printed as:

```java
ResponseType responseType = 
new ResponseType(requestMatchingCriteria, responseTemplate);
System.out.println(new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true).writeValueAsString(responseType));
```

The output for the message type registration JSON, `tutorial2-response3.json`, is:
```json
{
  "category" : {
    "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.OrMatcher",
    "matchers" : [ {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "GET"
      }
    }, {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "POST"
      }
    } ]
  },
  "response-template" : {
    "body" : "",
    "status" : 403,
    "thinktime" : 0,
    "type" : "STATIC"
  }
}
```

Using curl, the VL can be configured for the message type as follows:

```bash
curl --data @tutorial2-response3.json --header 'Content-Type: application/json' http://localhost:8080/virtuoso-listener-2.0/admin/register-response
```

All registered message and response types can be confirmed by running:

```bash
curl http://localhost:8080/virtuoso-listener-2.0/admin/configuration
```

### Step 3: Send messages to the VL
#### 1. Sending the following request:
```bash
curl -vv -X POST "http://localhost:8080/virtuoso-listener-2.0/service/register?username=username&password-md5=c8fed00eb2e87f1cee8e90ebbe870c190ac3848c"
```

will get a 200 response with body:

```html
<html>
	<body>
		Registration successful!
	</body>
</html>
```

This means that:
- The incoming request was matched against message **type 1**, as it was a POST request,  its path included the `/register` string and its username and password-md5 matched the provided regular expression.
- The unique 'id' for that message was correctly extracted.
- The incoming request was matched against response **type 1** and the static response was correctly returned.

#### 2. Sending the following request:

```bash
curl -vv -X GET --header "Authentication-Key:byBwYXBhcyBvIHBheHlz" "http://localhost:8080/virtuoso-listener-2.0/service/search"
```

will get a 200 response with an empty body. This means that:
- The incoming request was not matched against message **type 1**, as it is not a POST request.
- The incoming request was matched against message **type 2**, as it is a GET request and has a header with 'Authentication-Key' whose value matches the provided regular expression.
- The unique 'id' for that message was correctly extracted.
- The incoming request was not matched against response **type 1**, as it is not a POST request.
- The incoming request was matched against response **type 2**, as it is a GET request and has a header with 'Authentication-Key' whose value matches the provided regular expression and the '200' response with an empty body was returned.

#### 3. Sending the following request:

```bash
curl -vv -X POST "http://localhost:8080/virtuoso-listener-2.0/service/register?username=username&password-md5=c8fed00eb2e87f1cee--e90ebbe870c190ac3848c"
```

will get a 403 response with an empty body. This means that:
- The incoming request was not matched against message **type 1**, as its 'password-md5' parameter did not match the provided regular expression.
- The incoming request was not matched against message **type 2**, as it is not a GET request.
- The incoming request was matched against message **type 3**, as a POST request.
- The unique 'id' for that message was correctly extracted.
- The incoming request was not matched against response **type 1**, as its 'password-md5' parameter did not match the provided regular expression.
- The incoming request was matched against response **type 3**, as a POST request and the '403' response with an empty body was returned.

### Step 4: Search for stored messages 
Defining the search criteria, 'tutorial2-search.json', as:

```json
{
    "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
    "matchers" : [ {
      "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      "matcher-target" : {
        "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "GET"
      }
    } ]
  }
```

then, having received at least one GET message (of message type 2) in Step 3, the VL will respond to:

```bash
curl -X POST --data @tutorial2-search.json --header 'Content-Type: application/json' http://localhost:8080/virtuoso-listener-2.0/admin/search
```

with:

```json
[
  {
    "loggedDate":1444822863654,
    "method":"GET",
    "pathInfo":"/search",
    "url":"http://localhost:8080/virtuoso-listener-2.0/service/search",
    "parameters":{},
    "headers":{
      "user-agent":"curl/7.35.0",
      "host":"localhost:8080",
      "accept":"*/*",
      "authentication-key":"hfdhbyBwYXBhcyBvIHBheHlz"
    },
    "remoteAddress":"127.0.0.1",
    "body":""
  }
]
```