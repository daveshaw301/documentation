# Usage
## High-level description
Using the VL stub server comprises three stages:

1. Users configure the VL by:
	- registering matching criteria for incoming messages (message types);
	- defining how to extract unique identifiers from these messages (message types); and,
	- optionally, registering custom response templates for incoming messages (response types).

2. When messages are received, the VL:
	- searches for a matching registered message type;
	- extracts unique identifiers based on the message types extraction rules;
	- stores the message in the message store; and,
	- sends a default response or generates a custom response, if the message matches the criteria of a response type.

3. Users can manage the VL by:
	- viewing all registered message types, registered, response types, and stored messages;
	- searching for a specific stored message or for messages that fulfil certain criteria; and,
	- de-registering message types or response types and deleting stored messages.


## Configuring the VL
The typical use case of the VL comprises of registering acceptable message and response types:

1. Message types are configured by:

	i. defining the criteria to which incoming messages should adhere in order to be added to the store, e.g. all POST requests, or all messages that contain the URL path `/foo/bar`, or all messages with the parameter 'key' equal to '100', etc.
	
	ii. defining extraction rules for values that would uniquely identify incoming messages in the store, e.g. a regular expression to extract an id from the body of an acceptable incoming request.
	
2. Response types are optionally configured by:

	i. defining the criteria to which incoming messages should adhere for a customised response to be sent, e.g. all POST requests, or all messages that contain the URL path `/foo/bar`, or all messages with the parameter 'key' equal to '100', etc.
	
	ii. defining the customised response template, e.g. response body, response status code, and/or potential dynamic parameters in response body, and/or artificial delay before sending the response. 

The VL is configured via POST requests whose payload consists of serialised Java classes in JSON. These classes define the values and parameters for the various matching criteria, extraction rules, and response templates. Users can define the JSON content either by serialising Plain Old Java Objects (POJOs) of the VL library or by filling in the corresponding strings. Either way, the configuration options passed as JSON will be converted to Java Objects on the VL side and will subsequently be used to enforce matching and extraction rules on incoming messages. This approach allows for new matching and extraction rules to be passed and used by the VL without modifying the listener's core logic.


### Registering Message Types
Registering message types comprises two parts:

1. defining the matching criteria to which incoming messages should adhere in order to be added to the VL message store; and,
2. defining the rules to extract information that could uniquely identify an incoming message in the store, e.g. an id from the incoming message.

#### Defining matching criteria for a message type
The matching criteria currently cover method types, URL paths, SOAP actions, parameters, and headers and allow for exact matches, partial matches, or regular-expression-based matches. The complete list of available matching criteria can be found in the Matching Criteria section. 
An example of a (partial) JSON request to define matching criteria is the following:

```json
{
	"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
	"matchers" : [ {
  		"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
  		"matcher-target" : {
			"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
			"matcher-target" : "GET"
  		}
	}, {
		"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.HeaderMatcher",
		"matcher-target" : {
			"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ContainsStringMatcher",
			"matcher-target" : "Pass"
  		},
		"header-field-name" : "Test-Header"
	} ]
}
```
The corresponding POJO is defined as:

```java
Matcher<HttpServletRequest> messageMatchingCriteria = 
new AndMatcher<HttpServletRequest>(Method.GET, new HeaderMatcher("Test-Header", new ContainsStringMatcher("Pass")));
```

Based on these matching criteria, which only constitute a part of a JSON request for defining message types, an incoming message will be added to the store, if it meets both (AndMatcher) criteria below:
- Its method is GET.
- The message contains the header 'Test-Header' and this header's value contains the string 'Pass'.

#### Defining extraction rules
The second part of configuring message types relates to the way that information can be extracted from an incoming message to uniquely identify it in the VL store. This allows clients of the VL to subsequently query the store for the existence of a specific message.

The extraction rules currently cover method types, URL paths, SOAP actions, parameters, and headers and allow for the extraction of a value using regular expressions, Xpaths, or SOAP1.2 namespaces. The complete list of available extraction rules are defined using the classes of the table found in the Extraction Rules section. 

The definition of a message type extractor typically uses the CompositeExtractor class, which allows the combination of two classes: one that extracts information from a request as a string (e.g. the body of a request) and another one that extracts values from that string (e.g. the value that matches a regular expression in the extracted method body).  

An example of a (partial) JSON request to define a message type extractor is the following:

```json
{
	"extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.composite.CompositeExtractor",
    "left-func" : {
    	"extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.string.SingletonExtractor"
    },
    "right-func" : {
    	"extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.request.ParameterExtractor",
    	"parameter-key" : "id"
    }
}
```

The corresponding POJO can be defined as:

```java
Extractor extractorRules = 
new CompositeExtractor<>(new SingletonExtractor(), new ParameterExtractor("id"));
```

Based on this extraction rule, which only constitutes a part of a JSON request for defining message types, the unique value of an incoming message will be processed as follows:
- The value of the parameter named 'id' will be extracted.
- The value will be used as a whole (*SingletonExtractor*) without further processing.

#### Registering the VL message type
The matching criteria and extractor rules of the previous sections are combined in a single JSON body, message-type.json, as follows:

```json
{
	"category" : {
		"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
		"matchers" : [ {
			"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
			"matcher-target" : {
				"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
				"matcher-target" : "GET"
			}
    		}, {
      			"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.HeaderMatcher",
      			"matcher-target" : {
        			"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ContainsStringMatcher",
       			 	"matcher-target" : "Pass"
      			},
			"header-field-name" : "Test-Header"
		} ]
	},
	"id-extractor" : {
    	"extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.composite.CompositeExtractor",
    	"left-func" : {
      		"extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.string.SingletonExtractor"
    	},
    	"right-func" : {
      		"extractor-impl" : "uk.gov.homeoffice.virtuoso.services.message.extractor.request.ParameterExtractor",
      		"parameter-key" : "id"
    	}
  	}
}
```

The message-type.json above can then be sent as a POST request to `/admin/register-type`. Using the curl command-line tool this would look as follows:
```bash
curl --data @message-type.json --header 'Content-Type: application/json' http://localhost:8080/virtuoso-listener-2.0/admin/register-type
```

Using POJOs, the following object can be defined for the same purpose:
```java
Matcher<HttpServletRequest> messageMatchingCriteria = 
new AndMatcher<HttpServletRequest>(Method.GET, new HeaderMatcher("Test-Header", new ContainsStringMatcher("Pass")));

Extractor extractorRules = 
new CompositeExtractor<>(new SingletonExtractor(), new ParameterExtractor("id"));

MessageType messageType = new MessageType(messageMatchingCriteria, extractorRules);
```

This object could then be serialized and sent to the VL using an object mapper (sending not shown below):

```java
ObjectMapper objectMapper = new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true);

System.out.println("use the serialised message type (instance of MessageType) in requests to /admin/register-type to register a new message type");

System.out.println(objectMapper.writeValueAsString(messageType));
```

A successful registration will return the following message:
```html
virtuoso-listener: info: register-type: successfully registered message type
virtuoso-listener: info: register-type: try /admin/configuration for an updated list of registered message types
```
If a message type with the provided matching criteria already exists, then the request will fail with response:
```html
virtuoso-listener: error: register-type: message type already registered: no override
virtuoso-listener: info: register-type: try again with &override=true; or, 
virtuoso-listener: info: register-type: try /admin/configuration for a list of registered message types
```
To overcome this error, users can set an “override” parameter to “true” as follows:
```bash
curl --data @message-type.json --header 'Content-Type: application/json' http://localhost:8080/virtuoso-listener-2.0/admin/register-type?override=true
```
Sending this request will return a success message:
```html
virtuoso-listener: info: register-type: successfully re-registered message type
virtuoso-listener: info: register-type: try /admin/configuration for an updated list of registered message types
```
#### Checking registered message types
Users can see the supported message types that have been registered to a VL object by issuing a GET request to /admin/configuration and viewing the “registered-types” elements.
```bash
curl http://localhost:8080/virtuoso-listener-2.0/admin/configuration
```

### Registering Response Types
Registering response types comprises two parts:
1. defining the matching criteria to which incoming messages should adhere in order to generate a custom response; and,
2. defining the template of the custom response.

Response type definition is optional. If no response type is found for an incoming message, then the VL will return a 200 response code with an empty message body. Otherwise, the custom response will be used. In any case, an incoming message will only receive a successful response if it can be matched against a registered message type as explained in the Registering Message Types section.

#### Defining matching criteria for a response type
The matching criteria currently cover method types, URL paths, SOAP actions, parameters, and headers and allow for exact matches, partial matches, or regular-expression-based matches. The complete list of available matching criteria can be found in the Matching Criteria section. 

An example of a (partial) JSON request to define the matching criteria of a response type is the following:

```json
{
	"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
    "matchers" : [ {
    	"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
    	"matcher-target" : {
        	"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        	"matcher-target" : "GET"
      		}	
    	}, {
    	"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.PathInfoMatcher",
    	"matcher-target" : {
        	"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ContainsStringMatcher",
        	"matcher-target" : "/some/path"
      	}
	} ]
}
```

The corresponding POJO can be defined as:
```json
Matcher<HttpServletRequest> requestMatchingCriteria = 
new AndMatcher<HttpServletRequest>(Method.GET, new PathInfoMatcher(new ContainsStringMatcher("/some/path")));
```
Based on these matching criteria (which only comprise a part of a JSON request for defining response types) an incoming message that has already been matched against a registered message type will receive a customised response, if it meets both (AndMatcher) criteria below:

- Its method is `GET`
- The message path contains the `/some/path` string.

#### Defining response templates
The second part of configuring response types relates to the format of the response to an accepted incoming message. 
A 'response-template' field is required in the JSON request to define the response template for matching requests. The following table shows the supported fields:

|Name|Description|Values|Required|Default|
|----|-----------|------|--------|-------|
|type|Determines whether the response body will always be the same (static) or be adapted to include some variable value (dynamic)|STATIC or DYNAMIC|No|STATIC|
|body|String that will be returned as the body of the response<br><br>If the type is STATIC, then the same defined value will always be used.<br><br>If the type is DYNAMIC, then before returning the body, the VL will:<br><li> replace all strings of the format<br>`${TIMESTAMP:<SimpleDateFormat>}` in the response body with the current date formatted as defined in the SimpleDateFormat pattern.<br><br>replace all strings of the format <li>${REGEXP:<RegExp>} in the response body with the information extracted when the regular expression RegExp is applied on the incoming request body.|String that will be the response code|No|""|
|status|The response status code|An HTTP status code|No|200|
|thinktime|An artificial delay (in ms) to be applied before the VL sends the response to the incoming message of this response type|Milliseconds|No|0|

An example of a (partial) JSON request to define the response template of a response type is the following:

```json
{
    "body" : "<html><body>Custom response body</body></html>",
    "status" : 202,
    "thinktime" : 10,
    "type" : "STATIC"
}
```

The equivalent using POJOs is:
```
ResponseTemplate responseTemplate = 
new ResponseTemplate("<html><body>Custom response body</body></html>", 202, 1L, ResponseTemplate.Type.STATIC);
```

Based on this response template, which only comprises a part of a JSON request for defining response types, the response template for incoming message that match against a message type and against this response type will be as follows:

- Response code: `202`
- Response body: `<html><body>Custom response body</body></html>` (always the same)
- An artificial delay of `10ms` will be employed before sending the response.

#### Registering the VL response type
The matching criteria and response template are combined in a single JSON body, response-type.json, as follows:

```json
{
	"category" : {
    	"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
    	"matchers" : [ {
      		"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      		"matcher-target" : {
        		"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        		"matcher-target" : "GET"
      		}
    	}, {
      		"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.PathInfoMatcher",
      		"matcher-target" : {
        		"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ContainsStringMatcher",
        		"matcher-target" : "/some/path"
      		}
    	} ]
  	},
  	"response-template" : {
    	"body" : "<html><body>Custom response body</body></html>",
    	"status" : 202,
    	"thinktime" : 1,
    	"type" : "STATIC"
  	}
}
```

The response-type.json above can then be sent as a POST request to `/admin/register-response` as follows:
```bash
curl --data @response-type.json --header 'Content-Type: application/json' http://localhost:8080/virtuoso-listener-2.0/admin/register-response
```

Using POJOs, the following object can be defined for the same purpose:
```java
Matcher<HttpServletRequest> requestMatchingCriteria = 
new AndMatcher<HttpServletRequest>(Method.GET, new PathInfoMatcher(new ContainsStringMatcher("/some/path")));

ResponseTemplate responseTemplate = 
new ResponseTemplate("<html><body>Custom response body</body></html>", 202, 1L, ResponseTemplate.Type.STATIC);

ResponseType responseType = new ResponseType(requestMatchingCriteria, responseTemplate);
```
This object could then be serialized and sent to the VL using an object mapper (sending not shown below):
```java
ObjectMapper objectMapper = new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true);

System.out.println("use the serialised response type (instance of ResponseType) in requests to /admin/register-response to register a new response type");

System.out.println(objectMapper.writeValueAsString(responseType));
```
A successful registration will return the following message:

```html
virtuoso-listener: info: register-response: successfully registered response details
virtuoso-listener: info: register-response: try /admin/configuration for an updated list of registered response details
```

If a response type with the provided matching criteria already exists, then the request will fail with response:
```html
virtuoso-listener: error: register-response: response details already registered: no override
virtuoso-listener: info: register-response: try again with &override=true; or, 
virtuoso-listener: info: register-response: try /admin/configuration for a list of registered response details
```

To overcome this error, users can set an `override` parameter to `true` as follows:
```bash
curl --data @response-type.json --header 'Content-Type: application/json' http://localhost:8080/virtuoso-listener-2.0/admin/register-response?override=true
```
Sending this request will return a success message:
```html
virtuoso-listener: info: register-response: successfully re-registered response details
virtuoso-listener: info: register-response: try /admin/configuration for an updated list of registered response details
```

#### Checking registered response types
Users can see the supported response types that have been registered to a VL object by issuing a GET request to `/admin/configuration` and viewing the 'registered-responses' elements.
```bash
curl http://localhost:8080/virtuoso-listener-2.0/admin/configuration
```

## Sending messages to the VL
Any message that is sent to the VL and under the `/service/` path prefix is handled as an incoming message to the stub server. The high-level algorithm for all incoming messages is the following:

1. Find the first matching message type for the incoming message.
2. Extract IDs as defined by that message type.
3. Store information of the matched message to the VL message store.
4. Return a default response (200 with an empty body) or a customised one, if a relevant response type is defined.

For example, using the sample message type in the Configuring Message Types section the request:
```bash
curl -X GET --header "Test-Header:Passing" http://localhost:8080/virtuoso-listener-2.0/service/some/path?id=abc012 
```

will be matched against a rule that checks whether incoming requests contain the 'Test-Header' header and this header's value contains the 'Pass' string. The message will be uniquely identified by its 'id', due to the 'ParameterExtraction' rule of that message type, and added to the VL messages store.

Using the sample response type of the Configuring Response Types section the response to this incoming message, which uses a GET method, will be:
```html
<html><body>Custom response body</body></html> 
```
with a response code of `202`.

## Managing the message store
### Listing all configured types
All registered message and response types can be seen by sending a `GET` request to `/admin/configuration`.
```bash
curl http://localhost:8080/virtuoso-listener-2.0/admin/configuration
```
This returns a JSON response with the serialized message and response types. 

### Listing all messages
All messages that are logged in the message store can be seen by sending a GET request to `/admin/list`.
```bash
curl http://localhost:8080/virtuoso-listener-2.0/admin/list
```
This returns a JSON response with the details of all messages that currently reside in the VL store.

### Searching for messages
Users can search for messages that are logged in the VL store by sending a GET request to `/admin/search` and passing the relevant search criteria in JSON format. The JSON content will define matching criteria as a serialised `Matcher <HttpServletRequest>` class. Details of such classes can be found in the Matching Criteria section.

For example, searching for the JSON content base-search.json:
```json
{
"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
  "matcher-target" : {
    "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
    "matcher-target" : "GET"
  }
}
```
as
```bash
curl -X GET --data @base-search.json http://localhost:8080/virtuoso-listener-2.0/admin/search
```

will return a JSON response with the details of all messages in the store that have a GET method. 

More complex search criteria can be defined by using the **Matching Criteria** classes. For example, searching for the following JSON content, complex-search.json:
```json
{
  "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
  "matchers" : [ {
    "matcher-impl" :
"uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
    "matcher-target" : {
      "matcher-impl" :
"uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
      "matcher-target" : "GET"
    }
  }, {
    "matcher-impl" :
"uk.gov.homeoffice.virtuoso.services.message.matcher.request.ParameterMatcher",
"matcher-target" : {
"matcher-impl" :
"uk.gov.homeoffice.virtuoso.services.message.matcher.string.RegexpStringMatcher",
      "matcher-target" : "[a-z][a-z][a-z][0-9][0-9][0-9]"
    },
    "parameter-key" : "id"
  } ]
}
```
as
```bash
curl -X GET --data @complex-search.json http://localhost:8080/virtuoso-listener-2.0/admin/search
```
will return a JSON response with the details of all messages in the store that have a GET method and whose value for the `id` parameter matches the `[a-z][a-z][a-z][0-9][0-9][0-9]` regular expression.

If no messages match the search criteria, then a `200` response with an empty list `[]` will be returned.

### Querying for a specific message
Users can query whether a specific message is logged in the message store by sending a GET request to `/admin/query` and passing the relevant criteria in JSON format. The JSON content will contain criteria as a serialised MessageKey class. 

For example, querying for the following JSON content, query.json:

```json
{
  "category" : {
    "matcher-impl" :
"uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
    "matchers" : [ {
      "matcher-impl" :
"uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      "matcher-target" : {
        "matcher-impl" :
"uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "GET"
      }
    }, {
      "matcher-impl" :
"uk.gov.homeoffice.virtuoso.services.message.matcher.request.PathInfoMatcher",
      "matcher-target" : {
        "matcher-impl" :
"uk.gov.homeoffice.virtuoso.services.message.matcher.string.ContainsStringMatcher",
        "matcher-target" : "/some/path"
      }
    } ]
  },
  "id" : "abc012"
  }
}
```
with the request:
```bash
curl -X GET --data @query.json http://localhost:8080/virtuoso-listener-2.0/admin/query
```
will return a JSON representation for the details of the message that matches the query criteria, i.e. only matches messages with the GET method. If no message is matched, then a `404` response code will be sent with body:

```html
virtuoso-listener: error: query: requested message not in store
virtuoso-listener: info: query: try /admin/list for a list of stored messages
```
Messages are returned only upon exact matches of criteria. This means that if a stored message was matched against the criteria:
- method is GET and
- path contains `/some/path`
and the query's search criteria only include 'method is GET', then no results will be returned.

### De-registering a message type
Users can de-register a previously registered message type by sending a POST request to `/admin/de-register-type` and passing the relevant search criteria to identify the message type to be de-registered in JSON format. The JSON content will contain matching criteria as a serialised `Matcher<HttpServletRequest>` class as detailed in the Matching Criteria section.
For example, de-registering for the JSON content de-reg-type.json:
```json
{
"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
  "matcher-target" : {
    "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
    "matcher-target" : "GET"
  }
}
```
with the request:
```bash
curl -X GET --data @de-reg-type.json http://localhost:8080/virtuoso-listener-2.0/admin/de-register-type
```
will de-register the message type that accepts all messages that have a GET method. 

Upon successful de-registration, the VL returns a confirmation message as follows:
```html
virtuoso-listener: info: de-register-type: successfully de-registered message type
virtuoso-listener: info: de-register-type: try /admin/configuration for an updated list of registered message types
```

### De-registering a response type
Users can de-register a previously registered response type by sending a POST request to `/admin/de-register-response` and passing the relevant search criteria to identify the response type to be de-registered in JSON format. The JSON content will contain matching criteria as a serialised `Matcher<HttpServletRequest>` class as detailed in the Matching Criteria section.

For example, de-registering for the JSON content de-reg-response.json:
```json
{
"matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
  "matcher-target" : {
    "matcher-impl" : "uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
    "matcher-target" : "GET"
  }
}
```
with the request:
```bash
curl -X GET --data @de-reg-response.json http://localhost:8080/virtuoso-listener-2.0/admin/de-register-response
```
will de-register the response type that accepts all messages that have a GET method. 

Upon successful de-registration, the VL returns a confirmation message as follows:
```hml
virtuoso-listener: info: de-register-response: successfully de-registered responsevirtuoso-listener: info: de-register-response: try /admin/configuration for an updated list of registered responses
Removing messages from the store
```

Users can remove a message from the store by sending a POST request to `/admin/remove` and passing the relevant matching criteria to identify the message to be removed in JSON format. The JSON content will contain matching criteria as a serialised `Matcher<HttpServletRequest>` class as detailed in the Matching Criteria section.

For example, removing the message described by the JSON content message-remove.json:
```json
{
  "category" : {
    "matcher-impl" :
"uk.gov.homeoffice.virtuoso.services.message.matcher.composite.AndMatcher",
    "matchers" : [ {
      "matcher-impl" :
"uk.gov.homeoffice.virtuoso.services.message.matcher.request.MethodMatcher",
      "matcher-target" : {
        "matcher-impl" :
"uk.gov.homeoffice.virtuoso.services.message.matcher.string.ExactStringMatcher",
        "matcher-target" : "GET"
      }
    }, {
      "matcher-impl" :
"uk.gov.homeoffice.virtuoso.services.message.matcher.request.PathInfoMatcher",
      "matcher-target" : {
        "matcher-impl" :
"uk.gov.homeoffice.virtuoso.services.message.matcher.string.ContainsStringMatcher",
        "matcher-target" : "/some/path"
      }
    } ]
  },
  "id" : "abc012"
  }
```

using the request:
```bash
curl -X GET --data @message-remove.json http://localhost:8080/virtuoso-listener-2.0/admin/remove
```
will de-register the message that has a GET method, contains the path `/some/path` and has extracted unique id equal to `abc012`. 
Upon successful de-registration, the VL returns a confirmation message as follows:
```html
virtuoso-listener: info: remove: successfully removed stored message
```
If no such message is found, then the VL will return a 404 response as follows:
```html
virtuoso-listener: error: remove: requested message not in store
virtuoso-listener: info: remove: try /admin/list for a list of stored messages
```

### Deleting in bulk
Users can delete message types, response types, or stored messages in bulk, by sending a POST request to “/admin/purge”.
Sending the following request:
```bash
curl http://localhost:8080/virtuoso-listener-2.0/admin/purge
```
will delete all message types, response types, and stored messages on the VL.

Finer grained control of bulk deletion allows for passing parameters to define which components need to be purged:
- types={`TRUE`|`FALSE`} – message types (default `TRUE`)
- responses={`TRUE`|`FALSE`} – response types (default `TRUE`)
- store={`TRUE`|`FALSE`} – stored messages (default `TRUE`)

For example, the following request:
```bash
curl http://localhost:8080/virtuoso-listener-2.0/admin/purge?store=FALSE
```
will delete all message types and response types, but will retain any stored messages on the VL.

### Help
Users can view the help menu of the VL by sending a GET request to `/admin/help`:
```bash
curl http://localhost:8080/virtuoso-listener-2.0/admin/help
```
This returns a concise report of how to interact with the server.
​ 
### Matching Criteria
The matching rules that are currently supported by the VL are shown in the following table:

#### Table 1: Classes specifying the message information to match against
|Class<sup>1</sup>|Description|Parameters|
|-----------|-----------|----------|
|MethodMatcher|Match against the method type of the request, e.g. GET, POST, etc.|`matcher-target`: class which defines how to perform matching – see [Table 2](#table-2-classes-defining-how-to-perform-the-matching)|
|PathInfoMatcher|Match against the path of the request, e.g. “/some/path”.|`matcher-target`: class which defines how to perform matching – see [Table 2](#table-2-classes-defining-how-to-perform-the-matching)|
|Soap11ActionMatcher|Match against the action of a SOAP1.1 request.|`matcher-target`: class which defines how to perform matching – see [Table 2](#table-2-classes-defining-how-to-perform-the-matching)|
|Soap12ActionMatcher|Match against the action of a SOAP1.2 request.|`matcher-target`: class which defines how to perform matching – see [Table 2](#table-2-classes-defining-how-to-perform-the-matching)|
|ParameterMatcher|Match against the value of a specific parameter, e.g. the “id” parameter should be in the format 1xx.|`matcher-target`: class which defines how to perform matching – see Table 2<br>`parameter-key`: the name of the parameter on which to match the value|
|HeaderMatcher|Match against a header of the request.|`matcher-target`: class which defines how to perform matching – see [Table 2](#table-2-classes-defining-how-to-perform-the-matching)<br>`header-field-name`: the name of the request header on which to match the value|

<sup>1</sup> Under the `uk.gov.homeoffice.virtuoso.services.message.matcher.request` package.<br>

#### Table 2: Classes defining how to perform the matching
The following table shows the classes that can be used as matcher-targets when defining a matcher class:

|Class<sup>2</sup>|Description|Parameters|
|-------|-----------|----------|
|ExactStringMatcher|Successfully matches, if the checked value is exactly the same as the target value<br>e.g. both matcher-target and incoming value being “1234”.|`matcher-target`: value to match against|
|PrefixMatcher|Successfully matches, if the checked value starts with the target value<br>e.g. matcher-target being “12” and incoming value being “1234”.|`matcher-target`: value to match against|
|SuffixMatcher|Successfully matches, if the checked value ends with the target value<br>e.g. matcher-target being “34” and incoming value being “1234”.|`matcher-target`: value to match against|
|ContainsMatcher|Successfully matches, if the checked value contains the target value<br>e.g. matcher-target being “23” and incoming value being “1234”.|`matcher-target`: value to match against|
|RegexpMatcher|Successfully matches, if the checked value matches the regular expression of the target value<br>e.g. matcher-target being “1[0-9][0-9]4” and incoming value being “1234”.|`matcher-target`: regular expression to match against|

<sup>2</sup> Under the `uk.gov.homeoffice.virtuoso.services.message.matcher.string` package.

### Table 3: Classes used to combine matchers
The VL library offers helper classes to allow combinations of matcher classes. Specifically, the following classes are supported:

|Class<sup>3</sup>|Description|Parameters|
|--------|-----------|----------|
|AndMatcher|Combines any number of matchers. The resulting class matches successfully, only if the checked value matches successfully against all matchers.|matchers: list of matchers defined as in [Table 1](#table-1-classes-specifying-the-message-information-to-match-against)|
|OrMatcher|Combines any number of matchers. The resulting class matches successfully, of if the checked value matches successfully against any of the matchers.|matchers:  list of matchers defined as in [Table 1](#table-1-classes-specifying-the-message-information-to-match-against)|

<sup>3</sup> Under the `uk.gov.homeoffice.virtuoso.services.message.matcher.composite` package.

### Extraction Rules
The rules that are currently supported by the VL to extract information from a request are shown in the following table:

#### Table 4: Classes specifying which request information to extract
|Class<sup>4</sup>|Description|Parameters|
|------|-----------|----------|
|BodyExtractor|Extract the request body.||
|HeaderExtractor|Extract a request header.|header-field-name: name of header to extract|
|MethodExtractor|Extract the request method, e.g GET, POST, DELETE, etc.||
|ParameterExtractor|Extract a request parameter.|parameter-key: name of parameter to extract|
|PathInfoExtractor|Extract the request path, e.g. “/some/path/of/request”.||
|RemoteAddressExtractor|Extract the request source IP.||

<sup>4</sup>Under the `uk.gov.homeoffice.virtuoso.services.message.extractor.request` package.

#### Table 5: Classes defining string processing approaches
The following table shows the classes that can be used to process an extracted piece of information from a request:

|Class<sup>5</sup>|Description|Parameters|
|------|-----------|----------|
|SingletonExtractor|Return the entire input string||
|RegexpExtractor|Return all strings that match a regular expression. Templates are also supported, with $0$ being replaced by the entire matching string, and $1$ … $N$ by the n-th matched group in the regular expression.|regexp: regular expression to match against<br>template: (optional) template on which to use the matching strings from the regexp|
|XpathExtractor|The input string is processed as an XML and this extractor returns all strings that match an xpath in that string.|xpath-expression: xpath exression to be used to extract information from the input string|
|Soap12NamespaceExtractor|The input string is processed as a SOAP1.2 request body and this extractor returns the namespace of that request.||

<sup>5</sup>Under the `uk.gov.homeoffice.virtuoso.services.message.extractor.string` package.

#### Table 6: Composite extractor description
The VL library offers a helper class to combine the classes of [Table 4](#table-4-classes-specifying-which-request-information-to-extract) and [Table 5](#table-5-classes-defining-string-processing-approaches):

|Class<sup>6</sup>|Description|Parameters|
|--------|-----------|----------|
|CompositeExtractor|Combines two extractors. The “right-func” one defines how to extract information from a request and the “left-func” one how to process the extracted information.|right-func: an extractor from Table 4<br>left-func: an extractor from Table 5|

<sup>6</sup>Under the `uk.gov.homeoffice.virtuoso.services.message.extractor.composite` package.





