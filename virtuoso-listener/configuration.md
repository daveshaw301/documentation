## web.xml
The web.xml defines servlet context parameters. 

|Key|Description|Acceptable Values|Required|Default|
|---|-----------|-----------------|--------|-------|
|status.code.cannot.register|The HTTP status code used by the admin servlet to respond when a new message or response type cannot be registered successfully|Any valid HTTP status code|No|403|
|status.code.unknown.message.content|The HTTP status code used by the stub servlet to respond when a message of a unknown type arrives|Any valid HTTP status code|No|415|
|status.code.cannot.extract.id|The HTTP status code used by the stub servlet to respond when a message of a known type arrives, but its IDs cannot be obtained using the provided extractor|Any valid HTTP status code|No|403|

In Tomcat this is located at `webapps/virtuoso-listener/WEB-INF/web.xml` and the parameters can be uncommented and amended as necessary.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app version="2.4" xmlns="http://java.sun.com/xml/ns/j2ee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee
http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd">

    <servlet>
        <servlet-name>Virtuoso ListenerAdmin</servlet-name>
        <servlet-class>uk.gov.homeoffice.virtuoso.services.servlet.AdminServlet</servlet-class>
        <!--
                        <init-param>
                            <param-name>status.code.cannot.register</param-name>
                            <param-value>403</param-value>
                        </init-param>
        -->
    </servlet>

    <servlet>
        <servlet-name>Virtuoso ListenerStub</servlet-name>
        <servlet-class>uk.gov.homeoffice.virtuoso.services.servlet.StubServlet</servlet-class>
        <!--
                        <init-param>
                            <param-name>status.code.unknown.message.content</param-name>
                            <param-value>415</param-value>
                        </init-param>
        -->
        <!--
                        <init-param>
                            <param-name>status.code.cannot.extract.id</param-name>
                            <param-value>403</param-value>
                        </init-param>
        -->
    </servlet>

    <servlet-mapping>
        <servlet-name>Virtuoso ListenerAdmin</servlet-name>
        <url-pattern>/admin/*</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>Virtuoso ListenerStub</servlet-name>
        <url-pattern>/service/*</url-pattern>
    </servlet-mapping>

</web-app>
```