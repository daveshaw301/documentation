## Download
Download - https://gitlab.com/tdcs_framework/virtuoso-listener

## Build
To create the WAR file `mvn clean install`

## Deploying in Vitraux
The Virtuoso Listener is a servlet that can be deployed on any web container, such as Tomcat with Java1.8 All testing has been conducted using Tomcat 7 and 8.

If deploying to Tomcat, move the WAR to the `/webapps` directory of a Tomcat installation. Multiple instances of the VL can exist within Tomcat, rename the WAR for each instance so they can co-exist.

## Validation
Once the web application has been deployed, the installation can be validated (amend as necessary if you did not deploy virtuoso-listener.war or the default port 8080) as :
```bash
curl localhost:8080/virtuoso-listener/admin/help
```

Both validations will return a page listing the operations for the listener

```text
ADMIN OPERATIONS
NOTE: All operations to be preceded by the admin servlet path as set in web.xml.  Default /admin.
- GET /configuration          Display all registered message types and all registered response
                              details.
- GET /list                   Display the IDs of all received messages.
- GET /query                  Query a specific item from the store of received messages.
                              Use the following parameters to specify the required message:
    & method                  Mandatory.  HTTP method used to send the message.
    & action                  Mandatory.  Action associated to the message.
                              See description in STUB SERVICES section below.
    & mime-type               Mandatory.  Content-Type from the message's HTTP headers.
    & id                      Mandatory.  The unique ID of the message.
...
...
...
```
