The Virtuoso-Listener (VL) is a web-server offering a configurable stub functionality, paired with a storage facility that allows for tests to query whether messages matching given criteria have been received.

The stub server will only process messages that have been previously configured and reject all others.

Three main concepts are used in the VL:

1. The message type, which determines the matching criteria and extraction rules for messages to be added to the store
2. The list of stored messages, which contains information for messages that have been received.
3. The response type, which determines the response template for incoming messages.

![Figure 1: Virtuoso Listener Stub Workflow Architecture](virtuoso-listener/images/figure1.png)

Figure 1, which presents the high-level workflow of the VL, highlights the components that are mapped to these main concepts (1-3). Every time an incoming message arrives, the VL compares it against each of the currently registered message types, following these rules: 

- The first criteria to match, in the order in which they were registered, will be selected. To this respect, it is recommended that specific matching criteria e.g. exact matching ones, are registered before more general ones, e.g. "containing"-type match rules.
- If no matching criteria are found, the VL will respond with a 415 (HTTP Unsupported Media Type Error) status code by default. This status code is [configurable](/virtuoso-listener/configuration)

Once an incoming message is matched against a configured message type, then the extraction rules for that type are enforced in order for a unique identified, i.e. key, of that message to be extracted from the request. The following apply:

- If the extraction rules can be enforced, then the message is added to the VL message store for each of the identified (extracted) IDs, together with additional message details such as time of arrive, message body, source IP etc.
- If the extraction rules cannot be enforced, then the VL will respond with a 403 (HTTP Forbidden Error) status code by default. This status code is [configurable](/virtuoso-listener/configuration)

The VL then parses the registered response types to identify a match that would define a custom response to the income message. If no such response is found, the stub server responds with a 200 (HTTP OK) status code and an empty response body.