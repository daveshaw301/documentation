# The latest versions of the TDCS Framework

|Application|Version|Build|Test|
|-----------|--------------|-------|----|
|[Virtuoso](https://gitlab.com/tdcs_framework/virtuoso)|[v1.8](https://gitlab.com/tdcs_framework/virtuoso/repository/archive.tar.gz?ref=v1.8)|`mvn clean install`<br>Use locally or deploy to repository manager|Tests run as part of build<br>(requires internet connection)|
|[Virtuoso Listener](https://gitlab.com/tdcs_framework/virtuoso-listener)|[v2.0.1](https://gitlab.com/tdcs_framework/virtuoso-listener/repository/archive.tar.gz?ref=v2.0.1)|`mvn clean install`<br>Deploy `.war` to Tomcat webapps|`curl localhost:8080/virtuoso-listener/admin/help`<br>Functional tests within `/samples`|
|[Maestro](https://gitlab.com/tdcs_framework/maestro)|[v1.5.2](https://gitlab.com/tdcs_framework/maestro/repository/archive.tar.gz?ref=v1.5.2)|`mvn clean install`<br>Deploy to repository manager|-|
|[Vitraux](https://gitlab.com/tdcs_framework/vitraux)|[v1.4.5](https://gitlab.com/tdcs_framework/vitraux/repository/archive.tar.gz?ref=v1.4.5)|[Instructions to build server](https://gitlab.com/tdcs_framework/documentation/blob/master/vitraux/installation.md)|-|
|<li>Vitraux Virtuoso|-|`mvn clean -Pvirtuoso install`<br>Deploy `.war` to Tomcat webapps|`curl localhost:8080/virtuoso/VirtuosoServer?type=status`|
|<li>Vitraux Maestro|-|`mvn clean -Pmaestro install`<br>Deploy `.war` to Tomcat webapps|`curl localhost:8080/maestro/MaestroServer?type=status`|
|[Jenkins Plugin](https://gitlab.com/tdcs_framework/jenkins-plugin)|[v1.14.7](https://gitlab.com/tdcs_framework/jenkins-plugin/repository/archive.tar.gz?ref=v1.14.7)|`mvn clean –DskipTests install`<br>Upload `apt-plugin.hpi` to Jenkins|-|
For further details see the specific applicaiton documentation

## Application CHANGELOGS
- [Virtuoso](https://gitlab.com/tdcs_framework/virtuoso/blob/master/CHANGELOG)
- [Virtuoso Listener](https://gitlab.com/tdcs_framework/virtuoso-listener/blob/master/CHANGELOG)
- [Maestro](https://gitlab.com/tdcs_framework/maestro/blob/master/CHANGELOG)
- [Vitraux](https://gitlab.com/tdcs_framework/vitraux/blob/master/CHANGELOG)
- [Jenkins Plugin](https://gitlab.com/tdcs_framework/jenkins-plugin/blob/master/CHANGELOG)